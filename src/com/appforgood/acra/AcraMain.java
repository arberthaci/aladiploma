package com.appforgood.acra;

import android.app.Application;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

@ReportsCrashes(
		httpMethod = HttpSender.Method.PUT,
	    reportType = HttpSender.Type.JSON,
	    formUri = "http://54.76.96.180:5984/acra-effeci/_design/acra-storage/_update/report",
	    formUriBasicAuthLogin = "alMED",
	    formUriBasicAuthPassword = "!almed1"
	    //formKey = ""
)

public class AcraMain extends Application 
{
    @Override
    public void onCreate() 
    {
        super.onCreate();
        ACRA.init(this);
    }
}
