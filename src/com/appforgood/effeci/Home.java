package com.appforgood.effeci;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


//import com.appforgood.effeci.Home.LeDeviceListAdapter;
//import com.appforgood.effeci.DeviceScanActivity.ViewHolder;
import com.appforgood.effeciObjectModels.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.google.android.gms.location.LocationListener;

import android.app.Notification.Builder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class Home extends Activity implements OnClickListener, Runnable, 
OnDataPointListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
//GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener
{
	private static final long SCAN_PERIOD = 10000;
    static final int REQUEST_ENABLE_BT = 1;
	int flagServizi = 0;
	boolean notificheShPreferences = false;
	static String currentTime;
	static ArrayList<TimeTherapyObject> timeList = new ArrayList<TimeTherapyObject>();
	Calendar calendar;
	BroadcastReceiver mReceiver;
	String bacteriumId[];
	BluetoothDevice device;
	String bacteriumList;
	BacteriumObject bacteriumObject = new BacteriumObject();
	static BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	static boolean flag = false;
	Handler handler = null;
	static boolean foundBacterium = false;
	Timer timerBacterium = null;
	TimerTask doAsynchronousTask = null;
	Runnable tempRunnable = null;
	LinearLayout btnTerapie, btnDiario, btnProfilio;
	TextView textTerapie, textDiario, textProfilio;
	ImageView imageTerapie, imageDiario, imageProfilio;
	String[] dailyElementList = {"komente", "sekrecione","glicemi", "mushkerite", "temperatura", "hapat"};
	String[] bacterioList = {"asnjeri", "Pseudomonas aeruginosa","Staphilococcus aureus MSSA", "Staphilococcus aureus MRSA", "Achromobacter xylosoxidans",
			"Stenotrophomonas maltophilia", "Burkholderia cepacia","Streptococcus pneumoniae", "Escherichia coli", "Haemophilus influenzae",
			"Aspergillus fumigatus group", "Aspergillus terreus group", "Scedosporium spp"};
	String[] dailyElementPasiveFoto = {Integer.toString(R.drawable.notes), Integer.toString(R.drawable.emptyespettorato),Integer.toString(R.drawable.glicemia),
			Integer.toString(R.drawable.fevempty), Integer.toString(R.drawable.temperature),Integer.toString(R.drawable.passi)};
	String[] dailyElementActiveFoto = {Integer.toString(R.drawable.notes), Integer.toString(R.drawable.espettorato),Integer.toString(R.drawable.glicemia),
			Integer.toString(R.drawable.fev1), Integer.toString(R.drawable.temperature),Integer.toString(R.drawable.passi)};
	Context homeContext = null;
	static NotificationManager notificationManager;
	SharedPreferences pref=null;
	SharedPreferences pref1=null;
	private LeDeviceListAdapter mLeDeviceListAdapter;
    private boolean mScanning;
    private Handler mHandler;
    private UUID[] UUID = {BluetoothLeService.UUID_EFFECI};
	private static final String AUTH_PENDING = "auth_state_pending";
	private boolean authInProgress = false;
	public static GoogleApiClient mApiClient = null;
	protected static final String TAG = "Tag";
	private static final int REQUEST_OAUTH = 1;
	private BluetoothAdapter.LeScanCallback mLeScanCallback;
	private GoogleFitObject googleFit = new GoogleFitObject();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState); 
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.home);
		homeContext = this;
		
	    if (savedInstanceState != null) {
	        authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
	    }
	    
        final Handler handler = new Handler();
        /*handler.postDelayed(new Runnable() 
        {
          @Override
          public void run() 
          {
            pref1 = getApplicationContext().getSharedPreferences("SharedPreferencesEffeci",homeContext.MODE_PRIVATE);
            boolean diarioAutomatico = pref1.getBoolean("dati_salute", true);
            if(diarioAutomatico)
            {
                buildFitnessClient();
            }
          }
        }, 10000);*/
        /*pref = getApplicationContext().getSharedPreferences("SharedPreferencesEffeci", MODE_PRIVATE);
        boolean diarioAutomatico=pref.getBoolean("dati_salute", true);
        if(diarioAutomatico)
        {
            buildFitnessClient();
        }*/
		buildFitnessClient();
		
		btnTerapie = (LinearLayout) findViewById(R.id.llTerapie);
		btnDiario = (LinearLayout) findViewById(R.id.llDiario);
		btnProfilio = (LinearLayout) findViewById(R.id.llProfilio);
		
		textTerapie = (TextView) findViewById(R.id.tvTerapie);
		textDiario = (TextView) findViewById(R.id.tvDiario);
		textProfilio = (TextView) findViewById(R.id.tvProfilio);
		
		imageTerapie = (ImageView) findViewById(R.id.imgTerapie);
		imageDiario = (ImageView) findViewById(R.id.imgDiario);
		imageProfilio = (ImageView) findViewById(R.id.imgProfilio);
		
		btnTerapie.setOnClickListener(this);
		btnDiario.setOnClickListener(this);
		btnProfilio.setOnClickListener(this);
		
    	textTerapie.setTextColor(Color.parseColor("#a3a3a3"));
    	textDiario.setTextColor(Color.parseColor("#a3a3a3"));
    	textProfilio.setTextColor(Color.parseColor("#a3a3a3"));
    	
    	
		pref = getApplicationContext().getSharedPreferences("SharedPreferencesEffeci", MODE_PRIVATE);
		boolean firstOpen = pref.getBoolean("first_open", true);
		if(firstOpen)
		{
			try 
			{
				Calendar calendar = Calendar.getInstance(Locale.US);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
		        long timeofInstallation = calendar.getTimeInMillis();
				Editor editor = pref.edit();
				editor.putLong("time_of_installation", timeofInstallation);
				editor.commit();
	            DbUtils utilsDb = new DbUtils(this);
	            BacteriumOperations bacteriumOps = new BacteriumOperations(this);
	              
	              for(int i=1; i<=12; i++)
	              {
	            	  BacteriumObject bacteriumObject = new BacteriumObject();
	            	  bacteriumObject.setbacteriumName(bacterioList[i]);
	            	  bacteriumObject.setImagePath("effeci/path" + i);
	            	  long rowId = bacteriumOps.insertBacterium(bacteriumObject);
	            	  if (rowId < 0)
	            	  {
	            		  Toast.makeText(this, "Ka ndodhur nje gabim gjate shtimit te semundjes se re.", Toast.LENGTH_LONG).show();
	            	  }
	              }
	              bacteriumOps.close();
	              
	              DailyElementsOperations dailyElementsOp = new  DailyElementsOperations(this);
	              for(int i=0; i<6; i++)
	              {
		              DailyElements dailyElements = new DailyElements();
		              dailyElements.setName(dailyElementList[i]);
		              dailyElements.setActiveFoto(dailyElementActiveFoto[i]);
		              dailyElements.setPasiveFoto(dailyElementPasiveFoto[i]);
		              long RowId = dailyElementsOp.insertDailyElements(dailyElements);
		              
		              if (RowId < 0)
		        	  {
	        		      Toast.makeText(this, "Ka ndodhur nje gabim gjate shtimit te semundjes se re.", Toast.LENGTH_LONG).show();
		        	  } 
	              }
	              dailyElementsOp.close();  
	              utilsDb.close();
				}
			catch (Exception e)
			{
				e.printStackTrace();
				Toast.makeText(this, "Ka ndodhur nje gabim gjate krijimit te tabelave ne databaze. " + e.toString(), Toast.LENGTH_LONG).show();
			}
			Fragment attivaServizi = new ServiziLocalizzazione();
			if(attivaServizi != null)
	        {
				Editor editor = pref.edit();
				editor.putBoolean("first_open", false);
				editor.commit();
				
				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.flHome, attivaServizi);
				fragmentTransaction.commit();
	        }
		}
		else
		{
			onClick(btnDiario);
		}
		
		pref = getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		notificheShPreferences = pref.getBoolean("notifiche", false);
		editor.commit();
		
		if(notificheShPreferences == true )
		{
			Timer timer = new Timer();
			timer.schedule(new TimerTask()
			{
				@Override
				public void run() 
				{
				calendar = Calendar.getInstance();
					if(notificheShPreferences == true && (calendar.get(calendar.MINUTE) == 0 ||  calendar.get(calendar.MINUTE) == 30)) 
					{
						TimeTherapyOperations timeTherapyOperations = new TimeTherapyOperations(getApplicationContext());
						TimeTherapyObject timeTherapyObject = new TimeTherapyObject();
						TherapyOperations therapyOperations = new TherapyOperations(getApplicationContext());
						timeList = timeTherapyOperations.getTimeByDate(calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE));
						
						for (int i =0 ; i < timeList.size(); i++)  
						{ 
							String time = timeList.get(i).getTime();
							String timeShort = timeList.get(i).getTime();
							int medicinaleId = (int) timeList.get(i).getTherapyId();
							TherapyObject therapy = therapyOperations.getTherapyByID(medicinaleId);
							String medicinale = therapy.getTherapyName();
							calendar = Calendar.getInstance();
							currentTime = calendar.get(calendar.HOUR_OF_DAY) + ":" + calendar.get(calendar.MINUTE);
							int j = currentTime.length();
							if(currentTime.length()<5)
								{
									timeShort = timeShort.substring(0, 4);
								}
							int testTime = currentTime.compareTo(timeShort);
							if(testTime == 0 && timeList.get(i).getStatus() != 1)
							{
								Notify("alMED", "Merrni mjekimet e terapise  " + medicinale + " ne oren " + time, timeList.get(i).getTimeId(), homeContext);
							}
						}
					}
				};
			}, 0, 40000);
		}
		
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
		     // only for gingerbread and newer versions
			try{ 
				 mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
				        @Override
				        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
				            runOnUiThread(new Runnable() {
				                @Override
				                public void run() {
				                	
				                	foundBacterium = true;
				                    mLeDeviceListAdapter.addDevice(device);
				                    mLeDeviceListAdapter.notifyDataSetChanged();
				                }
				            });
				        }
				    };
		    	}
		    	catch(Exception e)
		    	{
		    		e.printStackTrace();
		    	} 
			
		  mHandler = new Handler();
		if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) 
		{
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) 
        {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        else
        {
       // mLeDeviceListAdapter.clear();
        boolean result = scanLeDevice(true);
	        if(foundBacterium)
	        {
	        	bacteriumNotify("alMED","U gjend pajisje qe permban aplikacionin dhe te njejten semundje me ju", Home.this);
	        }
        }
        }
	}
	@SuppressLint("NewApi")
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
	        // fire an intent to display a dialog asking the user to grant permission to enable it.
	        if (!mBluetoothAdapter.isEnabled()) {
	            if (!mBluetoothAdapter.isEnabled()) {
	                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
	            }
	        }
	       
	        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			     // only for gingerbread and newer versions
			// Initializes list view adapter.
	        mLeDeviceListAdapter = new LeDeviceListAdapter();
	       // setListAdapter(mLeDeviceListAdapter);
	        scanLeDevice(true);
	        }
		Intent x = getIntent();
		String notificationData[] = null;
		try 
		{
			notificationData = x.getAction().split("/");
		}
		catch(Exception e)
		{
			notificationData = null;
		}
		
		if (notificationData != null)
		{
			TimeTherapyOperations timeTherapy = new TimeTherapyOperations(homeContext);
			notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			if (notificationData[0].equalsIgnoreCase("0")) 
		    {
		    	timeTherapy.updateTimeStatusById(Integer.parseInt(notificationData[1]), 0);
		    	//Toast.makeText(homeContext, "No Clicked - " + notificationData[1], Toast.LENGTH_LONG).show();
		    	notificationManager.cancel(1);
		    	Intent restartIntent = new Intent(this, Home.class);
		    	restartIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //Set this flag
		    	startActivity(restartIntent);
		    	finishAffinity();
		    }
		    else if (notificationData[0].equalsIgnoreCase("1")) 
		    {
		    	timeTherapy.updateTimeStatusById(Integer.parseInt(notificationData[1]), 1);
		    	notificationManager.cancel(1);
		    	Intent restartIntent = new Intent(this, Home.class);
		    	restartIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //Set this flag
		    	startActivity(restartIntent);
		    	finishAffinity();
		    }
		    else if (notificationData[0].equalsIgnoreCase("-999"))
		    {
		    	try
		    	{
		    	final BluetoothDevice device = mLeDeviceListAdapter.getDevice(0);
		        if (device == null) return;
		        final Intent intent = new Intent(this, DeviceControlActivity.class);
		        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
		        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
		        if (mScanning) {
		            mBluetoothAdapter.stopLeScan(mLeScanCallback);
		            mScanning = false;
		        }
		        notificationManager.cancel(1);
		        startActivity(intent);
		    	}
		    	catch(Exception e)
		    	{
		    		e.printStackTrace();
		    	}
		    	 finishAffinity();
		    }
		}
	}
	@Override
	public void onClick(View view) 
	{
		Fragment fragment = null;
        switch (view.getId()) 
        {
        case R.id.llTerapie:
        	textTerapie.setTextColor(Color.parseColor("#fa8072"));
        	textDiario.setTextColor(Color.parseColor("#808080"));
        	textProfilio.setTextColor(Color.parseColor("#808080"));
        	
        	imageTerapie.setImageResource(R.drawable.terapie);
        	imageDiario.setImageResource(R.drawable.diario_gray);
        	imageProfilio.setImageResource(R.drawable.profilio_gray);
        	
        	fragment = new Terapie();
            break;
        case R.id.llDiario:
        	textTerapie.setTextColor(Color.parseColor("#808080"));
        	textDiario.setTextColor(Color.parseColor("#fa8072"));
        	textProfilio.setTextColor(Color.parseColor("#808080"));
        	
        	imageTerapie.setImageResource(R.drawable.terapie_gray);
        	imageDiario.setImageResource(R.drawable.diario);
        	imageProfilio.setImageResource(R.drawable.profilio_gray);
        	
        	fragment = new Diario();
            break;
        case R.id.llProfilio:
        	textTerapie.setTextColor(Color.parseColor("#808080"));
        	textDiario.setTextColor(Color.parseColor("#808080"));
        	textProfilio.setTextColor(Color.parseColor("#fa8072"));
        	
        	imageTerapie.setImageResource(R.drawable.terapie_gray);
        	imageDiario.setImageResource(R.drawable.diario_gray);
        	imageProfilio.setImageResource(R.drawable.profilio);
        	
        	fragment = new Profilio();
            break;
        }
        if(fragment != null)
        {
	   		FragmentManager fragmentManager = getFragmentManager();
		    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		    fragmentTransaction.replace(R.id.flHome, fragment);
		    fragmentTransaction.commit();
        }
	}
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private static void Notify(String notificationTitle, String notificationMessage, int timeTerapyId, Context context)
 	{
		int NOTIFICATION_ID = 1;
		notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent yesPressed = getPendingIntent(1, timeTerapyId, context);
		PendingIntent noPressed = getPendingIntent(0, timeTerapyId, context);
		Builder builder = new Notification.Builder(context);
		builder.setContentTitle(notificationTitle)
		    .setContentText(notificationMessage)
		    .setSmallIcon(R.drawable.almed_logo)
		    .setAutoCancel(true)
		    .setPriority(Notification.PRIORITY_HIGH)
		    .addAction(R.drawable.x_icon, "JO", noPressed)
		    .addAction(R.drawable.check_icon, "PO", yesPressed);
		Notification notification = new Notification.BigTextStyle(builder).bigText(notificationMessage).build();
		notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
		notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
		notification.defaults |= Notification.DEFAULT_SOUND; // Sound*/
		notificationManager.notify(1, notification);
 	}
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private void bacteriumNotify(String notificationTitle, String notificationMessage, Context context)
 	{
		int NOTIFICATION_ID = 1;
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	
		PendingIntent intent = getPendingIntent(-999, -999, context);
		
		Builder builder = new Notification.Builder(this);
		builder.setContentIntent(intent)
			.setContentTitle(notificationTitle)
		    .setContentText(notificationMessage)
		    .setSmallIcon(R.drawable.almed_logo)
		    .setAutoCancel(true)
		    .setPriority(Notification.PRIORITY_HIGH);
		   
		Notification notification = new Notification.BigTextStyle(builder).bigText(notificationMessage).build();
		//notification.flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification
		notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
		notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
		notification.defaults |= Notification.DEFAULT_SOUND; // Sound*/
		notificationManager.notify(1, notification);
 	}
	
	
	
	public static PendingIntent getPendingIntent(int buttonClicked, int timeTerapyId, Context context) 
	{
		Intent homeIntent = new Intent(context, Home.class);
		homeIntent.setAction(buttonClicked + "/" + timeTerapyId);
		return PendingIntent.getActivity(context, 0, homeIntent, 0);
	 }
	public NotificationManager getNotificationManager() 
	{
		return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	}

	    

	

	 @Override
	 public void onBackPressed() 
	 {
		 if (getFragmentManager().getBackStackEntryCount() == 0) 
	     {
	    	 AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	    builder.setMessage("Deshironi te dilni nga aplikacioni?")
	    	           .setCancelable(false)
	    	           .setPositiveButton("Po", new DialogInterface.OnClickListener()
	    	           {
	    	               public void onClick(DialogInterface dialog, int id) 
	    	               {
	    	                finish();
	    	               }
	    	           })
	    	           .setNegativeButton("Jo", new DialogInterface.OnClickListener() 
	    	           {
	    	               public void onClick(DialogInterface dialog, int id) 
	    	               {
	    	                    dialog.cancel();
	    	               }
	    	           });
	    	    AlertDialog alert = builder.create();
	    	    alert.show();
	     } else 
	     {
	         getFragmentManager().popBackStack();
	     }
	 }
	 
	 private boolean scanLeDevice(final boolean enable) {
		 boolean scanResult= false;
	        if (enable) {
	        	
	            // Stops scanning after a pre-defined scan period.
	            mHandler.postDelayed(new Runnable() {
	                @Override
	                public void run() {
	                    mScanning = false;
	                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
	                    //invalidateOptionsMenu();
	                }
	            }, SCAN_PERIOD);

	            mScanning = true;
	       scanResult = mBluetoothAdapter.startLeScan(UUID, mLeScanCallback);
	        } else {
	            mScanning = false;
	            mBluetoothAdapter.stopLeScan(mLeScanCallback);
	        }
	       // invalidateOptionsMenu();
	        return scanResult;
	    }
	
		 private class LeDeviceListAdapter extends BaseAdapter {
		        private ArrayList<BluetoothDevice> mLeDevices;
		        private LayoutInflater mInflator;

		        public LeDeviceListAdapter() {
		            super();
		            mLeDevices = new ArrayList<BluetoothDevice>();
		            mInflator = Home.this.getLayoutInflater();
		        }

		        public void addDevice(BluetoothDevice device) {
		            if(!mLeDevices.contains(device)) {
		                mLeDevices.add(device);
		            }
		        }

		        public BluetoothDevice getDevice(int position) {
		            return mLeDevices.get(position);
		        }

		        public void clear() {
		            mLeDevices.clear();
		        }

		        @Override
		        public int getCount() {
		            return mLeDevices.size();
		        }

		        @Override
		        public Object getItem(int i) {
		            return mLeDevices.get(i);
		        }

		        @Override
		        public long getItemId(int i) {
		            return i;
		        }
				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					// TODO Auto-generated method stub
					return null;
				}
		    }
		 static class ViewHolder {
		        TextView deviceName;
		        TextView deviceAddress;
		    }
		 @Override
		    protected void onPause() {
		        super.onPause();
		        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) 
		        {
		        scanLeDevice(false);
		        mLeDeviceListAdapter.clear();
		        }
		    }
		 
		/* @Override
		    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		        // User chose not to enable Bluetooth.
		        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
		            finish();
		            return;
		        }
		        super.onActivityResult(requestCode, resultCode, data);
		    }
		    */
		@Override
		public void run() 
		{
			// TODO Auto-generated method stub
		}
		
		private void buildFitnessClient() 
		{
	       	// Create the Google API Client
			mApiClient = new GoogleApiClient.Builder(this)
		            .addApi(Fitness.SENSORS_API)
		            .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
		            .addConnectionCallbacks(this)
		            .addOnConnectionFailedListener(this)
		            .build();
		}
		
		/*public void subscribe() 
	    {
	    	try 
	    	{
	        Fitness.RecordingApi.subscribe(googleAPIClient, DataType.TYPE_STEP_COUNT_DELTA)
	                .setResultCallback(new ResultCallback<Status>() 
	                {
	                    @Override
	                    public void onResult(Status status) 
	                    {
	                        if (status.isSuccess()) 
	                        {
	                            if (status.getStatusCode() == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) 
	                            {
	                                Log.i(TAG, "Existing subscription for activity detected.");
	                            } 
	                            else 
	                            {
	                                Log.i(TAG, "Successfully subscribed!");
	                            }
	                        } 
	                        else 
	                        {
	                            Log.i(TAG, "There was a problem subscribing.");
	                        }
	                    }
	        });
	    	}
	    	catch(Exception e)
	    	{
	    		Log.i(TAG, "Subscribed Error: " + e.toString());
	    	}
	    }*/

	    @Override
		public void onStop() 
	    {
	        super.onStop();
	        Fitness.SensorsApi.remove( mApiClient, this )
	                .setResultCallback(new ResultCallback<Status>() {
	                    @Override
	                    public void onResult(Status status) {
	                        if (status.isSuccess()) {
	                            mApiClient.disconnect();
	                        }
	                    }
	                });
	    }

	    @Override
		public void onActivityResult(int requestCode, int resultCode, Intent data) 
	    {
	    	 if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
		            finish();
		            return;
		        }
	         if( requestCode == REQUEST_OAUTH ) {
	             authInProgress = false;
	             if( resultCode == RESULT_OK ) {
	                 if( !mApiClient.isConnecting() && !mApiClient.isConnected() ) {
	                     mApiClient.connect();
	                 }
	             } else if( resultCode == RESULT_CANCELED ) {
	                 Log.e(TAG, "RESULT_CANCELED" );
	             }
	         } else {
	             Log.e(TAG, "requestCode NOT request_oauth");
	         }
	        super.onActivityResult(requestCode, resultCode, data);
	    }
	    
	    @Override
	    protected void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        outState.putBoolean(AUTH_PENDING, authInProgress);
	    }
	 
	    @Override
	    public void onConnectionFailed(ConnectionResult connectionResult) {
	        if( !authInProgress ) {
	            try {
	                authInProgress = true;
	                connectionResult.startResolutionForResult(Home.this, REQUEST_OAUTH);
	            } catch(IntentSender.SendIntentException e ) {
	     
	            }
	        } else {
	            Log.e(TAG, "authInProgress" );
	        }
	    }
		
		@Override
		public void onConnected(Bundle bundle) {
		    DataSourcesRequest dataSourceRequest = new DataSourcesRequest.Builder()
		            .setDataTypes( DataType.TYPE_STEP_COUNT_CUMULATIVE )
		            .setDataSourceTypes( DataSource.TYPE_RAW )
		            .build();
		 
		    ResultCallback<DataSourcesResult> dataSourcesResultCallback = new ResultCallback<DataSourcesResult>() {
		        @Override
		        public void onResult(DataSourcesResult dataSourcesResult) {
		            for( DataSource dataSource : dataSourcesResult.getDataSources() ) {
		                if( DataType.TYPE_STEP_COUNT_CUMULATIVE.equals( dataSource.getDataType() ) ) {
		                    registerFitnessDataListener(dataSource, DataType.TYPE_STEP_COUNT_CUMULATIVE);
		                }
		            }
		        }
		    };
		 
		    Fitness.SensorsApi.findDataSources(mApiClient, dataSourceRequest)
		            .setResultCallback(dataSourcesResultCallback);
		}

		@Override
		public void onConnectionSuspended(int cause) 
		{
			Log.i(TAG, "Connection suspended!");
		}
		
	    @Override
	    public void onDataPoint(DataPoint dataPoint) {
	    	//TODO ARBER => FINISHED
	    	pref1 = getApplicationContext().getSharedPreferences("SharedPreferencesEffeci",homeContext.MODE_PRIVATE);
            final String actualStepsDate = pref1.getString("actualStepsDate", "1993-04-02");
            try {
            	Calendar yesterdayDate = Calendar.getInstance();
            	yesterdayDate.add(Calendar.DATE, -1);
            	GoogleFitOperations googleFitOperation = new GoogleFitOperations(homeContext);
            	googleFit = googleFitOperation.getGoogleFitByDate(yesterdayDate.get(Calendar.YEAR)+ "-" +(yesterdayDate.get(Calendar.MONTH)+1) + "-" + yesterdayDate.get(Calendar.DATE));
            }
            catch(Exception e) {
            	e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            if (actualStepsDate.equalsIgnoreCase(calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE))) {
		        for( final Field field : dataPoint.getDataType().getFields() ) {
		            final Value value = dataPoint.getValue( field );
		            if (field.getName().equalsIgnoreCase("STEPS")) {
		            	int actualSteps = pref1.getInt("actualSteps", 0);
		            	int stepsReadedFromGoogleFit = 0;
		            	try {
		            		String stepsToString = String.valueOf(value);
		            		stepsReadedFromGoogleFit = Integer.parseInt(stepsToString);
		            	}
		            	catch(Exception e) {
		            		e.printStackTrace();
		            	}
		            	if (actualSteps <= stepsReadedFromGoogleFit) {
		            		actualSteps = stepsReadedFromGoogleFit;
		            	}
		            	else {
		            		actualSteps += stepsReadedFromGoogleFit;
		            	}
						Editor editor = pref1.edit();
						editor.putInt("actualSteps", actualSteps - googleFit.getSteps());
						editor.commit();
		            }
		            runOnUiThread(new Runnable() {
		                @Override
		                public void run() {
		                	Diario.updateListView(actualStepsDate);
		                    //Toast.makeText(getApplicationContext(), "Field: " + field.getName() + " Value: " + value, Toast.LENGTH_SHORT).show();
		                }
		            });
		        }
            }
            else if(actualStepsDate.equalsIgnoreCase("1993-04-02")) {
				Editor editor = pref1.edit();
				editor.putString("actualStepsDate", calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE));
				editor.commit();
            }
            else {
            	Editor editor = pref1.edit();
				editor.putString("actualStepsDate", calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE));
				editor.commit();
				//TODO ARBER => FINISHED
				int actualSteps = pref1.getInt("actualSteps", 0);
				try {
					googleFit.setDate(actualStepsDate);
					googleFit.setSteps(actualSteps);
					GoogleFitOperations googleFitOperation = new GoogleFitOperations(homeContext);
					googleFitOperation.insertGoogleFit(googleFit);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
            }
	    }
	    @Override
	    protected void onStart() {
	        super.onStart();
	        Log.i(TAG, "Connecting...");
	        mApiClient.connect();
	    }
	    
	    private void registerFitnessDataListener(DataSource dataSource, DataType dataType) {
	    	 
	        SensorRequest request = new SensorRequest.Builder()
	                .setDataSource( dataSource )
	                .setDataType( dataType )
	                .setSamplingRate( 3, TimeUnit.SECONDS )
	                .build();
	     
	        Fitness.SensorsApi.add( mApiClient, request, this )
	                .setResultCallback(new ResultCallback<Status>() {
	                    @Override
	                    public void onResult(Status status) {
	                        if (status.isSuccess()) {
	                            Log.e(TAG, "SensorApi successfully added" );
	                        }
	                    }
	                });
	    }
		
		public static void initializeNotification(boolean notify, Context context)
		{
			final boolean notificheShPreferences = notify;
			final Context thisContext = context;
			Timer timer = new Timer();
			timer.schedule(new TimerTask()
			{
				@Override
				public void run() 
				{
					Calendar calendar = Calendar.getInstance();
					if(notificheShPreferences == true && (calendar.get(calendar.MINUTE) == 0 ||  calendar.get(calendar.MINUTE) == 30)) 
					{
						TimeTherapyOperations timeTherapyOperations = new TimeTherapyOperations(thisContext);
						TimeTherapyObject timeTherapyObject = new TimeTherapyObject();
						TherapyOperations therapyOperations = new TherapyOperations(thisContext);
						timeList = timeTherapyOperations.getTimeByDate(calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE));
						
						for (int i =0 ; i < timeList.size(); i++)  
						{ 
							String time = timeList.get(i).getTime();
							String timeShort = timeList.get(i).getTime();
							int medicinaleId = (int) timeList.get(i).getTherapyId();
							TherapyObject therapy = therapyOperations.getTherapyByID(medicinaleId);
							String medicinale = therapy.getTherapyName();
							calendar = Calendar.getInstance();
							currentTime = calendar.get(calendar.HOUR_OF_DAY) + ":" + calendar.get(calendar.MINUTE);
							int j = currentTime.length();
							if(currentTime.length()<5)
							{
								timeShort = timeShort.substring(0, 4);
							}
							int testTime = currentTime.compareTo(timeShort);
							if(testTime == 0 && timeList.get(i).getStatus() != 1)
							{
								Notify("alMED", "Merrni mjekimet e terapise  " + medicinale + " ne oren " + time, timeList.get(i).getTimeId(), thisContext);
							}
						}
					}
				};
			}, 0, 40000);
		}
	}
		 
		 
