
package com.appforgood.effeci;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class DateTimePickerActivity extends Activity 
{
	private int year;
    private int month;
    private int day;
    private int startHour;
    private int endHour;
    private int startMinute;
    private int endMinute;
    static final int DATE_DIALOG_ID = 999;
    TimePickerDialog startTimePicker, endTimePicker, addTimePicker;
    
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		String pickerType = getIntent().getExtras().getString("picker_type");
		if (pickerType.equalsIgnoreCase("time")) 
		{
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);
			
			startTimePicker = new TimePickerDialog(DateTimePickerActivity.this,TimePickerDialog.THEME_DEVICE_DEFAULT_LIGHT,new TimePickerDialog.OnTimeSetListener() 
			{
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) 
                {
                	startHour = selectedHour;
                	startMinute = selectedMinute;
        			addTimePicker = new TimePickerDialog(DateTimePickerActivity.this,TimePickerDialog.THEME_DEVICE_DEFAULT_LIGHT, new TimePickerDialog.OnTimeSetListener() 
        			{
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) 
                        {
                        	endHour = selectedHour;
                        	endMinute = selectedMinute;
                            TerapieAdd.tvStartTime.setText(new StringBuilder().append(String.format("%02d", startHour)).append(":").append(String.format("%02d", startMinute)));
                            TerapieAdd.tvEndTime.setText(new StringBuilder().append(String.format("%02d", endHour)).append(":").append(String.format("%02d", endMinute)));
                            finish();
                        }
                    },
                    startHour, startMinute, true);
        			endTimePicker.setTitle("Zgjidh kohen perfundimtare");
        			endTimePicker.setButton(DialogInterface.BUTTON_NEGATIVE, "Anullo", new DialogInterface.OnClickListener() 
        			{
                        public void onClick(DialogInterface dialog, int which) 
                        {
                           if (which == DialogInterface.BUTTON_NEGATIVE) 
                           {
                        	   finish();
                           }
                        }
                      });
        			endTimePicker.show();
                }
            },
            hour, minute, true);
			startTimePicker.setTitle("Zgjidh kohen fillestare");
			startTimePicker.setButton(DialogInterface.BUTTON_NEGATIVE, "Anullo", new DialogInterface.OnClickListener() 
			{
                 public void onClick(DialogInterface dialog, int which) 
                 {
                    if (which == DialogInterface.BUTTON_NEGATIVE) 
                    {
                 	   finish();
                    }
                 }
               });
			startTimePicker.show();
		}
		else if (pickerType.equalsIgnoreCase("date"))
		{
			showDialog(DATE_DIALOG_ID);
		}
		else 
		{
			finish();
		}
	}
	
    @Override
    protected Dialog onCreateDialog(int id) 
    {
        switch (id)
        {
            case DATE_DIALOG_ID:
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
 
                DatePickerDialog dialog = new DatePickerDialog(this, DatePickerDialog.THEME_DEVICE_DEFAULT_LIGHT, datePickerListener, year, month ,day);
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Anullo", new DialogInterface.OnClickListener() 
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                       if (which == DialogInterface.BUTTON_NEGATIVE) 
                       {
                    	   finish();
                       }
                    }
                  });
                return dialog;
        }
        return null;
    }
 
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() 
    {
        public void onDateSet(DatePicker view, int selectedyear, int selectedMonth, int selectedDay) 
        {
        	year = selectedyear;
        	month = selectedMonth;
        	day = selectedDay;
        	
            TerapieAdd.etDate.setText(new StringBuilder().append(String.format("%02d", month + 1)).append("/").append(String.format("%02d", day)).append("/").append(String.format("%02d", year)));
            TerapieAdd.selectedDate = new StringBuilder().append(String.format("%02d", year)).append("-").append(String.format("%02d", month + 1)).append("-").append(String.format("%02d", day)).toString();
            finish();
        }
    };
}
