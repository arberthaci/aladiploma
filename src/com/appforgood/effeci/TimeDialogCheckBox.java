package com.appforgood.effeci;

import java.util.ArrayList;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TimeDialogCheckBox extends DialogFragment
{
	ArrayList orariSelectedItems;
	String pickTime = "Percaktoni orarin e marrjes se medikamenteve";
	CharSequence[] timeListChar ;
	LinearLayout timeContainer;
	public static boolean flagTime = false;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) 
	{
		timeListChar = new CharSequence[48];
		final boolean[] timeChecked = new boolean[48];

		for(int i = 0 ; i<24; i++)
		{
			String firstTime;
			String secondTime;
			if(i < 10) 
			{
				firstTime = "0"+String.valueOf(i)+":00";
				secondTime = "0"+String.valueOf(i)+":30";
			}
			else
			{
				firstTime = String.valueOf(i)+":00";
				secondTime = String.valueOf(i)+":30";
			}
			timeListChar[i*2] = firstTime;
			timeListChar[i*2 +1] = secondTime;
		}
		SharedPreferences preference = this.getActivity().getSharedPreferences("SharedPreferencesEffeciTimeCheckBox", Context.MODE_PRIVATE);
		final Editor editor = preference.edit();
		for(int i=0; i<48; i++)
		{
			timeChecked[i] = preference.getBoolean(String.valueOf(timeListChar[i]), false);
		}

		orariSelectedItems = new ArrayList();  
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(pickTime)
		.setMultiChoiceItems(timeListChar, timeChecked,
				new DialogInterface.OnMultiChoiceClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which,boolean isChecked) 
			{
				if (isChecked) 
				{
					orariSelectedItems.add(which);
					timeChecked[which] = true;
				}
				else if(!isChecked)
				{
					timeChecked[which] = false;
				} 
			}
		}
				)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int id) 
			{
				for(int i = 0; i < timeListChar.length; i++) 
				{
					editor.putBoolean(String.valueOf(timeListChar[i]), timeChecked[i]);
				}
				editor.commit();
				timeContainer = (LinearLayout) getActivity().findViewById(R.id.timeContainer);
				timeContainer.removeAllViews();
				int textBoxNumber = 0;
					for(int i=0; i<48; i++)
					{
						if(timeChecked[i] == true)
						{
						        // add text view
								if(textBoxNumber <3)
								{
						        TextView textView = new TextView(getActivity());
						        LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
					            params.setMargins(0, 0, 10, 0); 
					            textView.setLayoutParams(params);
					            textView.setText(timeListChar[i]);
						        textView.setTextColor(Color.parseColor("#636363"));
					            textView.setTypeface(null, Typeface.BOLD);
						        timeContainer.addView(textView);
								}
								textBoxNumber++;
								flagTime = true;
						}
					}
					if(textBoxNumber>3)
					{
						TextView textView = new TextView(getActivity());
				        LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			            params.setMargins(0, 0, 10, 0); 
			            textView.setLayoutParams(params);
			            textView.setText("...");
				        textView.setTextColor(Color.parseColor("#636363"));
			            textView.setTypeface(null, Typeface.BOLD);
				        timeContainer.addView(textView);
					}
					
			}
		})
		.setNegativeButton("Anullo", new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int id) 
			{
			}
		});
		return builder.create();
	}
	public static TimeDialogCheckBox newInstance() 
	{
		TimeDialogCheckBox check = new TimeDialogCheckBox();
		return check;
	}
}
