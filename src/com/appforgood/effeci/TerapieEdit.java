package com.appforgood.effeci;
 
import java.util.ArrayList;

import com.appforgood.effeciObjectModels.TherapyObject;
import com.appforgood.effeciObjectModels.TherapyOperations;
import com.appforgood.effeciObjectModels.TimeTherapyObject;
import com.appforgood.effeciObjectModels.TimeTherapyOperations;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TerapieEdit extends Fragment implements OnClickListener
{
	RelativeLayout btnBack;
	Button sospendiTerapia,terminaTerapia;
	Fragment fragment = null;
	public static int id = -1;
	int sospendi = 0;
	int termina = 2;
	TherapyOperations therapyOperations;
	TherapyObject therapy;
	TimeTherapyOperations timeOperations;
	ArrayList<TimeTherapyObject> timeObject;
	TimeTherapyObject result;
	TextView tvNote, bacteriumName, tvDataInizio, tvDurata, tvOgniQuantiGiorni , tvOrariAssunzione;
	View thisActualView;
	LinearLayout timeContainer;
	
	public TerapieEdit(int id)
	{
		this.id = id;
     }
	@Override
	   public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
	   {
			ActionBar thisActionBar = getActivity().getActionBar();
			thisActionBar.setDisplayShowHomeEnabled(false);
			thisActionBar.setDisplayShowTitleEnabled(false);
			LayoutInflater varInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View thisCustomView = varInflater.inflate(R.layout.terapia_krypto_actionbar, null);
			thisActualView = varInflater.inflate(R.layout.terapia_krypto, null);
			btnBack = (RelativeLayout) thisCustomView.findViewById(R.id.backTerapie);
			btnBack.setOnClickListener(this);
			tvNote = (TextView) thisActualView.findViewById(R.id.tvNote);
			bacteriumName = (TextView) thisActualView.findViewById(R.id.terapia_krypto); 
			tvDataInizio = (TextView) thisActualView.findViewById(R.id.tvDataInizio); 
			tvDurata = (TextView) thisActualView.findViewById(R.id.tvDurata); 
			tvOgniQuantiGiorni = (TextView) thisActualView.findViewById(R.id.tvOgniQuantiGiorni); 
			tvOrariAssunzione = (TextView) thisActualView.findViewById(R.id.tvOrariAssunzione); 
			
			sospendiTerapia = (Button) thisActualView.findViewById(R.id.sospendi_terapia); 
			terminaTerapia = (Button) thisActualView.findViewById(R.id.termina_terapia); 
			sospendiTerapia.setOnClickListener(this);
			terminaTerapia.setOnClickListener(this);
			
			therapyOperations = new TherapyOperations(getActivity());
			therapy = therapyOperations.getTherapyByID(id);
			
			timeOperations = new TimeTherapyOperations(getActivity());
 			timeObject = timeOperations.getTimeByID(id);
			
		    tvNote.setText(String.valueOf(therapy.getNotes()));
		    bacteriumName.setText(therapy.getTherapyName());
		    String[] startDate = therapy.getStartDate().split("-");
		    tvDataInizio.setText(" "+ startDate[1] + "/" + startDate[2] + "/" + startDate[0]); 
		    tvDurata.setText(String.valueOf(" "+therapy.getDuration()));
		    tvOgniQuantiGiorni.setText(" "+ String.valueOf(therapy.getFrequency()));
		    
		    timeContainer = (LinearLayout) thisActualView.findViewById(R.id.timeContainer);
			timeContainer.removeAllViews();
			timeContainer = (LinearLayout) thisActualView.findViewById(R.id.timeContainer);
			timeContainer.addView(tvOrariAssunzione);
			int i;
		    for (i = 0; i < timeObject.size(); i++)
		    {
		    	if(i<3)	
		    	{
		    		result = timeObject.get(i);
					TextView textView = new TextView(getActivity());
					LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				    params.setMargins(0, 0, 10, 0); 
				    textView.setLayoutParams(params);
				    textView.setText(result.getTime());
				    textView.setTextColor(Color.parseColor("#808080"));
				    textView.setTypeface(null, Typeface.BOLD);
					timeContainer.addView(textView);
		    	}
		    }
		    	if(i>3)
		    	{
		    		TextView textView = new TextView(getActivity());
			        LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		            params.setMargins(0, 0, 10, 0); 
		            textView.setLayoutParams(params);
		            textView.setText("...");
			        textView.setTextColor(Color.parseColor("#669933"));
		            textView.setTypeface(null, Typeface.BOLD);
			        timeContainer.addView(textView);
		    	}
		    	
		    if (therapy.getStatus() == 0) 
		    {
		    	sospendiTerapia.setBackgroundResource(R.drawable.button_shape);
				sospendiTerapia.setText("Riaktivizo terapine");
				sospendiTerapia.setTextColor(Color.parseColor("#808080"));
		    }
		    else if (therapy.getStatus() == 1)
		    {
		    	sospendiTerapia.setBackgroundResource(R.drawable.button_shape1);
				sospendiTerapia.setText("Anullo terapine");
				sospendiTerapia.setTextColor(Color.parseColor("#669933"));
		    }
			thisActionBar.setCustomView(thisCustomView);
			thisActionBar.setDisplayShowCustomEnabled(true);
			
			return thisActualView;
	   }  
	@Override
	public void onClick(View view) 
	{
		long rowId;
		switch(view.getId())
		{
		case R.id.backTerapie:
		{
		getActivity().getFragmentManager().popBackStack();
		
		break;
		}
		case R.id.sospendi_terapia:
		{
			if(therapy.getStatus() == 1)
			{
				therapy.setStatus(0);
				rowId = therapyOperations.updateTherapy(therapy);
				if (rowId > -1){
					sospendiTerapia.setBackgroundResource(R.drawable.button_shape);
					sospendiTerapia.setText("Riaktivizo terapine");
					sospendiTerapia.setTextColor(Color.parseColor("#808080"));
					Toast.makeText(getActivity(), "Terapia u anullua", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(getActivity(), "Ka ndodhur nje gabim", Toast.LENGTH_SHORT).show();
				}
			}
			else if (therapy.getStatus() == 0)
			{
				therapy.setStatus(1);
				rowId= therapyOperations.updateTherapy(therapy);
				if (rowId > -1){
					sospendiTerapia.setBackgroundResource(R.drawable.button_shape1);
					sospendiTerapia.setText("Anullo terapine");
					sospendiTerapia.setTextColor(Color.parseColor("#669933"));
					Toast.makeText(getActivity(), "Terapia u aktivizua", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(getActivity(), "Ka ndodhur nje gabim", Toast.LENGTH_SHORT).show();
				}
			}
			break;}
		case R.id.termina_terapia:
		{
			therapy.setStatus(2);
			rowId = therapyOperations.updateTherapy(therapy);
			if (rowId > -1){
				onClick(btnBack);
				Toast.makeText(getActivity(), "Terapia ka perfunduar", Toast.LENGTH_SHORT).show();
			}
			else
			{
				Toast.makeText(getActivity(), "Ka ndodhur nje gabim", Toast.LENGTH_SHORT).show();
			}
			break;
			}
		}
	}
}
