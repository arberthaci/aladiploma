package com.appforgood.effeci;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.appforgood.effeciObjectModels.*;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.listener.ChartTouchListener.ChartGesture;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataDeleteRequest;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.location.LocationListener;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Diario extends DialogFragment implements OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener,OnChartGestureListener
{
	static ArrayList<List> list = new ArrayList<List>(); 
	static ArrayList<DiarioObject> dailyElementsList;
	static ArrayList<DiarioObject> dailyTimes = new ArrayList<DiarioObject>() ;
	static ListView lvMenu;
	static DailyElementsOperations dailyElements;
	static ArrayList<DailyElements> elementsNames;
	ArrayList<DiarioObject> diarioObjectList = new ArrayList<DiarioObject>();
	ImageButton bAdd, btnLeftDate, btnRightDate;
	TextView tvToday, tvDiarioExtrInfo;
	Calendar calendar;
	View itemView;
	static DailyCheckUpOperations dailyCheckUp;
	static Activity thisActivity;
	SharedPreferences pref = null;
	SharedPreferences pref1 =null;
	boolean notificheShPreferences = false;
	String currentTime;
	ArrayList<TimeTherapyObject> timeList = new ArrayList<TimeTherapyObject>();
	public static List<String> listItem = new ArrayList<String>();
	
	public static String stepNumber = "";
	private static final int REQUEST_OAUTH = 1;
	private static final String DATE_FORMAT = "yyyy.MM.dd HH:mm:ss";
	private static final String AUTH_PENDING = "auth_state_pending";
	protected static final String TAG = "Tag";
	private boolean authInProgress = false;
	static boolean flagDialog = true;
	private ArrayList<BarEntry> entries;
	private ArrayList<String> labels;
	BarChart chart;
	int numberOfDaysChart = 0;
	private ProgressDialog dialog;
	
	@Override
   	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
   	{
		View actualView = null;
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
		{
			Calendar datasetCalendar = Calendar.getInstance(Locale.US);
	        long endTime = datasetCalendar.getTimeInMillis();
	        datasetCalendar.set(Calendar.HOUR_OF_DAY, 0);
	        datasetCalendar.set(Calendar.MINUTE, 0);
	        long startTime = datasetCalendar.getTimeInMillis();
            //new InsertAndVerifyDataTask(startTime, endTime).execute();
	        //TODO ARBER
	        /*SharedPreferences pref1 = getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
            boolean diarioAutomatico=pref1.getBoolean("dati_salute", false);
            if(diarioAutomatico)
            {
            	if (Home.mApiClient != null) {
            		new InsertAndVerifyDataTask(startTime, endTime).execute();
            	}
            }*/
			actualView = inflater.inflate(R.layout.diario, container, false);
			thisActivity = getActivity();
			ActionBar actionBar = getActivity().getActionBar();
			actionBar.setDisplayShowHomeEnabled(false);
			actionBar.setDisplayShowTitleEnabled(false);
			LayoutInflater inflaterLayout = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View customView = inflaterLayout.inflate(R.layout.diario_actionbar, null);
			itemView = inflaterLayout.inflate(R.layout.diario_listview_single_item, null);
			tvToday = (TextView) customView.findViewById(R.id.tvDiarioToday);

			tvDiarioExtrInfo = (TextView) itemView.findViewById(R.id.tvDiarioExtrInfo);
			btnLeftDate = (ImageButton) customView.findViewById(R.id.btnLeftDate);
			btnRightDate = (ImageButton) customView.findViewById(R.id.btnRightDate);
			btnLeftDate.setOnClickListener(this);
			btnRightDate.setOnClickListener(this);
			
			calendar = Calendar.getInstance(Locale.US);
			int monthInt = calendar.get(Calendar.MONTH);
			tvToday.setText(calendar.get(Calendar.DATE) + " " + getMonthByInt(monthInt));

			actionBar.setCustomView(customView);
			actionBar.setDisplayShowCustomEnabled(true);

			lvMenu = (ListView) actualView.findViewById(R.id.lvDiarioList);
			bAdd = (ImageButton) getActivity().findViewById(R.id.bDiarioAdd);
	
			bAdd.setOnClickListener(this);
			
			dailyElements = new DailyElementsOperations(thisActivity);
			dailyCheckUp = new DailyCheckUpOperations(thisActivity);
			dailyElementsList = new ArrayList<DiarioObject>();
			elementsNames = new ArrayList<DailyElements>();
			
			updateListView(calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE));
			SharedPreferences pref = thisActivity.getSharedPreferences("SharedPreferencesEffeci", thisActivity.MODE_PRIVATE);
			String dateToBeDisplayed = pref.getString("date_to_be_displayed", "-1");
	
			if(dateToBeDisplayed.equalsIgnoreCase("-1"))
			{
				dateToBeDisplayed = calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE);
			}
			else
			{
				String[] dateChunk = dateToBeDisplayed.split("-");
				tvToday.setText(dateChunk[2] + " " + getMonthByInt(Integer.parseInt(dateChunk[1])-1));
				Editor editor = pref.edit();
				editor.putString("date_to_be_displayed", "-1");
				editor.commit();
			}
			
			updateListView(dateToBeDisplayed);
		}
		else
		{
			flagDialog = true;
			actualView = inflater.inflate(R.layout.diario_land, container, false);
			thisActivity = getActivity();
			ActionBar actionBar = getActivity().getActionBar();
			actionBar.setDisplayShowHomeEnabled(false);
			actionBar.setDisplayShowTitleEnabled(false);
			LayoutInflater inflaterLayout = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View customView = inflaterLayout.inflate(R.layout.diario_actionbar, null);
			itemView = inflaterLayout.inflate(R.layout.diario_listview_single_item, null);
			tvToday = (TextView) customView.findViewById(R.id.tvDiarioToday);
			tvDiarioExtrInfo = (TextView) itemView.findViewById(R.id.tvDiarioExtrInfo);
			btnLeftDate = (ImageButton) customView.findViewById(R.id.btnLeftDate);
			btnRightDate = (ImageButton) customView.findViewById(R.id.btnRightDate);
			calendar = Calendar.getInstance(Locale.US);
			int monthInt = calendar.get(Calendar.MONTH);
			tvToday.setText(calendar.get(Calendar.DATE) + " " + getMonthByInt(monthInt));

			actionBar.setCustomView(customView);
			actionBar.setDisplayShowCustomEnabled(true);
			
			entries = new ArrayList();
			labels = new ArrayList<String>();
	        chart = (BarChart) actualView.findViewById(R.id.chart);
	        pref = getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
	        long timeOfInstallation = pref.getLong("time_of_installation", 0);
	        long actualTime = calendar.getTimeInMillis();
	        numberOfDaysChart = (int) (((actualTime - timeOfInstallation) / 86400000) + 1);
	        //TODO ARBER
	        /*if (Home.mApiClient != null) {
		        for (long iterator = timeOfInstallation; iterator<=actualTime; iterator+=86400000)
		        {
		        	new PrepareDataForChart(iterator, iterator + 86394000).execute();
		        }
	        }*/
	        ArrayList<GoogleFitObject> googleFitList = new ArrayList();
	        try {
	        	GoogleFitOperations googleFitOperations = new GoogleFitOperations(thisActivity);
	        	googleFitList = googleFitOperations.getAllGoogleFit();
	        	pref1 = thisActivity.getSharedPreferences("SharedPreferencesEffeci",thisActivity.MODE_PRIVATE);
	        	GoogleFitObject tempGoogleFit = new GoogleFitObject();
	        	tempGoogleFit.setDate(pref1.getString("actualStepsDate", "1993-04-02"));
	        	tempGoogleFit.setSteps(pref1.getInt("actualSteps", 0));
	        	googleFitList.add(tempGoogleFit);
	        }
	        catch(Exception e) {
	        	e.printStackTrace();
	        }
            for(GoogleFitObject iterator : googleFitList) 
            {
            	try 
            	{
            		entries.add(new BarEntry(Float.parseFloat(String.valueOf(iterator.getSteps())), entries.size()));
            	}
            	catch (Exception e)
            	{
            		entries.add(new BarEntry(0f, entries.size()));
            	}
            	labels.add(iterator.getDate());
            }

			//Calendar tempCalendar = Calendar.getInstance(Locale.US);
			//long actualTime = tempCalendar.getTimeInMillis();
			if (entries.size() == numberOfDaysChart)
			{
				BarDataSet dataset = new BarDataSet(entries, "hapat");
				dataset.setColor(Color.parseColor("#FFD801"));
				
				BarData data = new BarData(labels, dataset);
				chart.setData(data);
				chart.setVisibleXRangeMaximum(15);

				chart.setDescription("Pershkrimi");
				chart.animateY(5000);
				chart.moveViewToX(entries.size() - 15);
			}
	        //chart.setOnChartGestureListener(this);
	        SharedPreferences pref = getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
			boolean notificheShPreferences = pref.getBoolean("notifiche", false);
			if (notificheShPreferences)
			{
				Home.initializeNotification(notificheShPreferences, getActivity());
			}
		}
		return actualView;
   	}  
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
	    super.onConfigurationChanged(newConfig);
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) 
	    {
	        Fragment fragment = null;
	        fragment = getFragmentManager().findFragmentById(R.id.flHome);
	        final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
	        fragmentTransaction.detach(fragment);
	        fragmentTransaction.attach(fragment);
	        fragmentTransaction.commit();
	    } 
	    else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
	    {
 	        Fragment fragment = null;
	        fragment = getFragmentManager().findFragmentById(R.id.flHome);
	        final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
	        fragmentTransaction.detach(fragment);
	        fragmentTransaction.attach(fragment);
	        fragmentTransaction.commit();
	    }
	}
	
	public static void updateListView(String date) 
	{
		if(dailyElementsList != null) {
			dailyElementsList.clear();
			elementsNames.clear();
			dailyTimes.clear();
			elementsNames = dailyElements.getAllElements();
			for(int i=0; i<elementsNames.size(); i++)
			{
				DiarioObject dailyObject = new DiarioObject();
				DailyCheckUp checkUp = dailyCheckUp.getCheckUp(date, elementsNames.get(i).getDailyElementsId());
				dailyObject.setName(elementsNames.get(i).getName());
				dailyObject.setValue(checkUp.getValue());
				if(checkUp.getValue().equalsIgnoreCase("0") || checkUp.getValue().equalsIgnoreCase(""))
				{
					dailyObject.setFoto(elementsNames.get(i).getPasiveFoto());
				}
				else
				{
					dailyObject.setFoto(elementsNames.get(i).getActiveFoto());	
				}
				dailyObject.setId(checkUp.getDailyCheckUpId());
				dailyObject.setDailyelementid(elementsNames.get(i).getDailyElementsId());
				if (dailyObject.getName().equalsIgnoreCase("HAPAT")) {
					Calendar actualDate = Calendar.getInstance();
					if (date.equalsIgnoreCase(actualDate.get(Calendar.YEAR)+ "-" +(actualDate.get(Calendar.MONTH)+1) + "-" + actualDate.get(Calendar.DATE))) {
						SharedPreferences pref = thisActivity.getSharedPreferences("SharedPreferencesEffeci", thisActivity.MODE_PRIVATE);
						dailyObject.setValue(String.valueOf(pref.getInt("actualSteps", 0)));
					}
					else {
						try {
							GoogleFitOperations googleFitOperation = new GoogleFitOperations(thisActivity);
		            		GoogleFitObject googleFit = googleFitOperation.getGoogleFitByDate(date);
		            		dailyObject.setValue(String.valueOf(googleFit.getSteps()));
						}
						catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
				dailyElementsList.add(dailyObject);
				dailyTimes = dailyElements.getTherapyTimesByDate(date); 
				/*Calendar updateCalendar = Calendar.getInstance(Locale.US);
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
				Date dateCalendar;
				long endTime = 0;
				long startTime = 0;
				try {
					dateCalendar = format.parse(date);
					updateCalendar.setTime(dateCalendar);;
					updateCalendar.set(Calendar.HOUR_OF_DAY, 23);
					updateCalendar.set(Calendar.MINUTE, 59);
			        endTime = updateCalendar.getTimeInMillis();
			        updateCalendar.set(Calendar.HOUR_OF_DAY, 0);
			        updateCalendar.set(Calendar.MINUTE, 0);
			        startTime = updateCalendar.getTimeInMillis();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				//new InsertAndVerifyDataTask(startTime, endTime).execute();
			}
	        dailyElementsList.addAll(dailyTimes);
	        list = new ArrayList<List>((Collection<? extends List>) dailyElementsList);
	
			try 
			{
				EffeciListAdapter myAdapter = new EffeciListAdapter(date,thisActivity, list, 1);
				lvMenu.setAdapter(myAdapter);
			}
			catch(Exception e) 
			{
				Toast.makeText(thisActivity, e.toString(), Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onClick(View arg0) 
	{
		Fragment fragment = null;
		Calendar datasetCalendar = null;
		long startTime = 0;
		long endTime = 0;
		
        switch (arg0.getId()) 
        {
        case R.id.bDiarioAdd:
        	fragment = new Calendario();
            break;
        case R.id.btnLeftDate:
        	calendar.add(Calendar.DATE, -1);
    		int monthIntLeft = calendar.get(Calendar.MONTH);
    		tvToday.setText(calendar.get(Calendar.DATE) + " " + getMonthByInt(monthIntLeft));
    		String previousDate = calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE);
    		updateListView(previousDate);
            break;
        case R.id.btnRightDate:
        	calendar.add(Calendar.DATE, 1);
    		int monthIntRight = calendar.get(Calendar.MONTH);
    		tvToday.setText(calendar.get(Calendar.DATE) + " " + getMonthByInt(monthIntRight));
    		String nextDate = calendar.get(Calendar.YEAR)+ "-" +(calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE);
    		updateListView(nextDate);
            break;
        }
        
        if(fragment != null)
        {
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.flHome, fragment).addToBackStack("");;
			fragmentTransaction.commit();
        }
	}
	
	private String getMonthByInt(int month) 
	{
        String monthString = "Gabim";
	    DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(Locale.US);
	    String[] allMonths = dateFormatSymbols.getMonths();
	    if (month >= 0 && month <= 11 ) 
	    {
	    	monthString = allMonths[month];
	    }
	    return monthString;
	}

		//TODO ARBER
	    /*private static class InsertAndVerifyDataTask extends AsyncTask<Void, Void, Void> 
	    {
	    	private long startTime, endTime;
	    	private InsertAndVerifyDataTask(long paramStartTime, long paramEndTime)
	    	{
	    		startTime = paramStartTime;
	    		endTime = paramEndTime;
	    	}
	    	
	        @Override
			protected void onPreExecute() 
	        {
				super.onPreExecute();
			}

			protected Void doInBackground(Void... params) 
			{
	            try
	            {
				DataSet dataSet = insertFitnessData(startTime, endTime);
	            Log.i(TAG, "Inserting the dataset in the History API - Diario InsertAndVerifyDataTask");
	            com.google.android.gms.common.api.Status insertStatus = Fitness.HistoryApi.insertData(Home.mApiClient, dataSet).await(5, TimeUnit.SECONDS);
	            if (!insertStatus.isSuccess()) 
	            {
	                Log.i(TAG, "There was a problem inserting the dataset.");
	                return null;
	            }
	            Log.i(TAG, "Data insert was successful!");
	            DataReadRequest readRequest = queryFitnessData(startTime, endTime);
	            DataReadResult dataReadResult = Fitness.HistoryApi.readData(Home.mApiClient, readRequest).await(5, TimeUnit.SECONDS);
	            printData(dataReadResult);
	            }
	            catch(Exception e)
	            {
	            	e.printStackTrace();
	            }

	            return null;
	        }
	    }*/
	    //TODO ARBER
	    /*private class PrepareDataForChart extends AsyncTask<Void, Void, DataReadResult> 
	    {
	    	private long startTime, endTime;
	    	
	    	private PrepareDataForChart(long paramStartTime, long paramEndTime)
	    	{
	    		startTime = paramStartTime;
	    		endTime = paramEndTime;
	    	}
	    	
	        @Override
			protected void onPreExecute() 
	        {
				super.onPreExecute();
				if (flagDialog)
	    		{
	    			dialog = ProgressDialog.show(getActivity(), "", "Ne pritje...", true, true); 
	    			flagDialog = false;
	    		}
			}

			protected DataReadResult doInBackground(Void... params) 
			{
	            DataSet dataSet = insertFitnessData(startTime, endTime);
	            Log.i(TAG, "Inserting the dataset in the History API - Diario PrepareDataForChart");
	            com.google.android.gms.common.api.Status insertStatus = Fitness.HistoryApi.insertData(Home.mApiClient, dataSet).await(10, TimeUnit.SECONDS);
	            if (!insertStatus.isSuccess()) 
	            {
	                Log.i(TAG, "There was a problem inserting the dataset.");
	                return null;
	            }
	            Log.i(TAG, "Data insert was successful!");
	            DataReadRequest readRequest = queryFitnessData(startTime, endTime);
	            DataReadResult dataReadResult = Fitness.HistoryApi.readData(Home.mApiClient, readRequest).await(10, TimeUnit.SECONDS);
	            
	            return dataReadResult;
	        }

			@Override
			protected void onPostExecute(DataReadResult dataReadResult) 
			{
				super.onPostExecute(dataReadResult);
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				if (dataReadResult != null) {
					if (dataReadResult.getBuckets().size() > 0) 
			        {
			            Log.i(TAG, "Number of returned buckets of DataSets is: " + dataReadResult.getBuckets().size());
			                   
			            for (Bucket bucket : dataReadResult.getBuckets()) 
			            {
			                List<DataSet> dataSets = bucket.getDataSets();
			                for (DataSet dataSet : dataSets) 
			                {
			        	        for (DataPoint dp : dataSet.getDataPoints()) 
			        	        {
			        	            for(com.google.android.gms.fitness.data.Field field : dp.getDataType().getFields()) 
			        	            {
			        	            	try 
			        	            	{
			        	            		entries.add(new BarEntry(Float.parseFloat(dp.getValue(field).toString()), entries.size()));
			        	            	}
			        	            	catch (Exception e)
			        	            	{
			        	            		entries.add(new BarEntry(0f, entries.size()));
			        	            	}
			        	            	labels.add(dateFormat.format(startTime));
			        	            }
			        	        }
			                }
			            }   
			        }
				}
				Calendar tempCalendar = Calendar.getInstance(Locale.US);
				long actualTime = tempCalendar.getTimeInMillis();
				if (entries.size() == numberOfDaysChart)
				{
					BarDataSet dataset = new BarDataSet(entries, "passi");
					dataset.setColor(Color.parseColor("#808080"));
					
					BarData data = new BarData(labels, dataset);
					chart.setData(data);
					chart.setVisibleXRangeMaximum(15);
					
					if (dialog.isShowing()) 
					{
		                dialog.dismiss();
		            }
					chart.setDescription("Pershkrimi");
					chart.animateY(5000);
					chart.moveViewToX(entries.size() - 15);
				}
			}
	    }*/
	    private static DataSet insertFitnessData(long startTime, long endTime) 
	    {
	        // Create a data source
	        DataSource dataSource = new DataSource.Builder()
	                .setAppPackageName(thisActivity)
	                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
	                .setName(TAG + " - step count")
	                .setType(DataSource.TYPE_RAW)
	                .build();
	      
	        // Create a data set
	        int stepCountDelta = 0;
	        DataSet dataSet = DataSet.create(dataSource);
	        DataPoint dataPoint = dataSet.createDataPoint().setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
	        dataPoint.getValue(com.google.android.gms.fitness.data.Field.FIELD_STEPS).setInt(stepCountDelta);
	        dataSet.add(dataPoint);

	        return dataSet;
	    }

	    private static DataReadRequest queryFitnessData(long startTime, long endTime) 
	    {
	        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
	        Log.i(TAG, "Range Start: " + dateFormat.format(startTime));
	        Log.i(TAG, "Range End: " + dateFormat.format(endTime));

	        DataReadRequest readRequest = new DataReadRequest.Builder()
	                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
	                .bucketByTime(1, TimeUnit.DAYS)
	                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
	                .build();

	        return readRequest;
	    }
	    
	    private static void printData(DataReadResult dataReadResult) 
	    {
	        if (dataReadResult.getBuckets().size() > 0) 
	        {
	            Log.i(TAG, "Number of returned buckets of DataSets is: "
	                    + dataReadResult.getBuckets().size());
	            for (Bucket bucket : dataReadResult.getBuckets()) 
	            {
	                List<DataSet> dataSets = bucket.getDataSets();
	                for (DataSet dataSet : dataSets) 
	                {
	                    dumpDataSet(dataSet);
	                }
	            }
	            
	        } else if (dataReadResult.getDataSets().size() > 0) 
	        {
	            Log.i(TAG, "Number of returned DataSets is: "
	                    + dataReadResult.getDataSets().size());
	            for (DataSet dataSet : dataReadResult.getDataSets()) 
	            {
	                dumpDataSet(dataSet);
	            }
	        }
	    }

	    private static void dumpDataSet(DataSet dataSet) 
	    {
	        Log.i(TAG, "Data returned for Data type: " + dataSet.getDataType().getName());
	        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

	        for (DataPoint dp : dataSet.getDataPoints()) 
	        {
	            Log.i(TAG, "Data point:");
	            Log.i(TAG, "\tType: " + dp.getDataType().getName());
	            Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
	            Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getTimestamp(TimeUnit.MILLISECONDS)));
	            for(com.google.android.gms.fitness.data.Field field : dp.getDataType().getFields()) 
	            {
	                Log.i(TAG, "\tField: " + field.getName() + " Value: " + dp.getValue(field));
	                stepNumber = dp.getValue(field).toString();
	            }
	        }
	    }

	    @Override
		public void onConnectionFailed(ConnectionResult result) 
		{
			Log.i(TAG, "Connection Failed!");
		}

		@Override
		public void onConnected(Bundle connectionHint) 
		{
			Log.i(TAG, "Connection succed!");
		}

		@Override
		public void onConnectionSuspended(int cause) 
		{
			Log.i(TAG, "Connection suspended!");
		}

		@Override
		public void onLocationChanged(Location location) 
		{
			Log.i(TAG, "Location changed!");
		}
		
		@Override
        public void onChartScale(MotionEvent me, float scaleX, float scaleY) 
		{
			me.setLocation(scaleX, 0);
			Toast.makeText(thisActivity,"Clicked",Toast.LENGTH_SHORT).show();
        }

		  @Override
	      public void onChartTranslate(MotionEvent me, float dX, float dY) {
	          // TODO Auto-generated method stub
	      }
	      @Override
	      public void onChartSingleTapped(MotionEvent me) {
	          // TODO Auto-generated method stub
	      }
	      @Override
	      public void onChartLongPressed(MotionEvent me) {
	          // TODO Auto-generated method stub
	      }
	      @Override
	      public void onChartFling(MotionEvent me1, MotionEvent me2,
	              float velocityX, float velocityY) {
	          // TODO Auto-generated method stub
	      }
	      @Override
	      public void onChartDoubleTapped(MotionEvent me) {
	          // TODO Auto-generated method stub
	      }
	      public void onChartGestureStart(MotionEvent me, ChartGesture lastPerformedGesture)
	      {
	    	  // TODO Auto-generated method stub
	      }
	      public void onChartGestureEnd(MotionEvent me, ChartGesture lastPerformedGesture)
	      {
	    	  // TODO Auto-generated method stub
	      }
	      
}
