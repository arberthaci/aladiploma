package com.appforgood.effeci;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import com.appforgood.effeciObjectModels.CalendarObject;
import com.appforgood.effeciObjectModels.DailyElementsOperations;
import com.appforgood.effeciObjectModels.DiarioObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Calendario extends Fragment implements OnClickListener 
{
	public static ListView lvMenu;
	private int totalVisibleItems = 0;
	RelativeLayout btnBack;
	Calendar calendarBackward, calendarForward, calendaractual;
	static ArrayList<CalendarObject> eachDay;
	List<String> elementsItem = new ArrayList<String>();
	ArrayList<List> listTexts = new ArrayList<List>();
	static Activity thisActivity;
	static FragmentManager fragmentManager;
	public static TextView tvMonth;
	DailyElementsOperations dailyElementsOperations;
	EffeciListAdapter myAdapter;
	
	@Override
   	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
   	{
		View actualView = inflater.inflate(R.layout.calendario, container, false);
		thisActivity = getActivity();
		fragmentManager = getFragmentManager();
		ActionBar actionBar = getActivity().getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater inflaterLayout = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = inflaterLayout.inflate(R.layout.calendario_actionbar, null);
	    tvMonth = (TextView) actualView.findViewById(R.id.tvMonthCalendario);
	    actionBar.setCustomView(customView);
	    actionBar.setDisplayShowCustomEnabled(true);
		lvMenu = (ListView) actualView.findViewById(R.id.lvCalendarioList);
		btnBack = (RelativeLayout) getActivity().findViewById(R.id.backCalendario);
		btnBack.setOnClickListener(this);
		dailyElementsOperations = new DailyElementsOperations(getActivity());
		calendarBackward = Calendar.getInstance(Locale.US);
		calendarForward = Calendar.getInstance(Locale.US);
		calendaractual = Calendar.getInstance(Locale.US);
		eachDay = new ArrayList<CalendarObject>();
		calendarForward.add(Calendar.DATE, 1);
		showPreviousMonth();
		lvMenu.setOnScrollListener(new OnScrollListener() 
		{
	        private int currentVisibleItemCount;
	        private int currentScrollState;
	        private int currentFirstVisibleItem;
	        private int totalItem;

	        @Override
	        public void onScrollStateChanged(AbsListView view, int scrollState) 
	        {
	            // TODO Auto-generated method stub
	            this.currentScrollState = scrollState;
	            this.isScrollCompleted();               
	        }

	        @Override
	        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) 
	        {
	            // TODO Auto-generated method stub
	            this.currentFirstVisibleItem = firstVisibleItem;
	            this.currentVisibleItemCount = visibleItemCount;
	            totalVisibleItems = this.currentVisibleItemCount;
	            this.totalItem = totalItemCount;
	        }

	        private void isScrollCompleted() {
	            if (totalItem - currentFirstVisibleItem == currentVisibleItemCount && this.currentScrollState == SCROLL_STATE_IDLE) 
	            {
	            	showPreviousMonth();
	            }
	            else if (currentFirstVisibleItem == 0 && this.currentScrollState == SCROLL_STATE_IDLE)
	            {
	            	showNextMonth();
	            }
	        }
		});
		
		return actualView;
   	}  
	@Override 
	public void onClick(View arg0) 
	{
		if (arg0.getId() == R.id.backCalendario) 
		{
			Fragment fragment = null;
			fragment = new Diario();
		    if(fragment != null)
		    {
				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.flHome, fragment).addToBackStack("");;
				fragmentTransaction.commit();
		    }
		}
	}
	
	public static void goToSelectedDate(String selectedDate)
	{
		SharedPreferences pref = thisActivity.getSharedPreferences("SharedPreferencesEffeci", thisActivity.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putString("date_to_be_displayed", selectedDate);
		editor.commit();
		Fragment fragment = null;
		fragment = new Diario();
	    if(fragment != null)
	    {
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.flHome, fragment).addToBackStack("");;
			fragmentTransaction.commit();
	    }
	}
	
	private String getMonthByInt(int month) 
	{
        String monthString = "ERROR";
	    DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(Locale.US);
	    String[] allMonths = dateFormatSymbols.getMonths();
	    if (month >= 0 && month <= 11 ) 
	    {
	    	monthString = allMonths[month];
	    }
	    return monthString;
	}
	
	private void showPreviousMonth()
	{
		int goToIndex = eachDay.size();
		dailyElementsOperations = new DailyElementsOperations(getActivity());
	    for(int j=31; j>0; j--)
		{
			CalendarObject singleDay = new CalendarObject();
			String previousDate = calendarBackward.get(Calendar.YEAR)+ "-" +(calendarBackward.get(Calendar.MONTH)+1) + "-" + calendarBackward.get(Calendar.DATE);
			singleDay.setDate(calendarBackward.get(Calendar.DATE));
			singleDay.setFullDate(previousDate);
			if (singleDay.getDate() == calendarBackward.getActualMaximum(Calendar.DAY_OF_MONTH))
			{
				singleDay.setLastDayOfMonth(true);
			} 
			else 
			{
				singleDay.setLastDayOfMonth(false);
			}
			singleDay.setMonthName(calendarBackward.getDisplayName(Calendar.MONTH,Calendar.LONG, Locale.US));
			ArrayList<String> fotoList = dailyElementsOperations.getElementsByDate(previousDate);
			ArrayList<DiarioObject> fotoListTherapy = new ArrayList<DiarioObject>();
			fotoListTherapy = dailyElementsOperations.getTherapyTimeByDate(previousDate);
			if(fotoListTherapy.size() != 0) 
			{
					 if(fotoListTherapy.get(0).getFoto().equals("0"))
				     {
					    fotoList.add( Integer.toString(R.drawable.terapia_anomalia));
				     }
					 else if(fotoListTherapy.get(0).getFoto().equals("1"))
					 {
						fotoList.add( Integer.toString(R.drawable.terapie_1 ));
					 }	 
					 else if(fotoListTherapy.get(0).getFoto().equals("2"))
					 {
						fotoList.add( Integer.toString(R.drawable.terapie_completata));
					 }	
		    }
			if (fotoList != null)
			{
				singleDay.setFotoList(fotoList);
			}
			eachDay.add(singleDay);
			calendarBackward.add(Calendar.DATE, -1);
		}
	    dailyElementsOperations.close();
		listTexts = new ArrayList<List>((Collection<? extends List>) eachDay);
		
		try {
			myAdapter = new EffeciListAdapter(calendaractual.get(Calendar.YEAR)+ "-" +(calendaractual.get(Calendar.MONTH)+1) + "-" + calendaractual.get(Calendar.DATE), getActivity(), listTexts, 3);
			lvMenu.setAdapter(myAdapter);
			myAdapter.notifyDataSetChanged();
			if (goToIndex == 0) {
				lvMenu.setSelection(goToIndex);
			}
			else {
				lvMenu.setSelection(goToIndex - totalVisibleItems + 2);
			}
		}
		catch(Exception e)
		{
			Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}
	
	private void showNextMonth()
	{
		dailyElementsOperations = new DailyElementsOperations(getActivity());
	    for(int j=31; j>0; j--)
		{
			CalendarObject singleDay = new CalendarObject();
			String previousDate = calendarForward.get(Calendar.YEAR)+ "-" +(calendarForward.get(Calendar.MONTH)+1) + "-" + calendarForward.get(Calendar.DATE);
			singleDay.setDate(calendarForward.get(Calendar.DATE));
			singleDay.setFullDate(previousDate);
			if (singleDay.getDate() == calendarForward.getActualMaximum(Calendar.DAY_OF_MONTH))
			{
				singleDay.setLastDayOfMonth(true);
			} 
			else 
			{
				singleDay.setLastDayOfMonth(false);
			}
			singleDay.setMonthName(calendarForward.getDisplayName(Calendar.MONTH,Calendar.LONG, Locale.US));
			ArrayList<String> fotoList = dailyElementsOperations.getElementsByDate(previousDate);
			ArrayList<DiarioObject> fotoListTherapy = new ArrayList<DiarioObject>();
			fotoListTherapy = dailyElementsOperations.getTherapyTimeByDate(previousDate);
			if(fotoListTherapy.size() != 0) 
			{
					 if(fotoListTherapy.get(0).getFoto().equals("0"))
				     {
					    fotoList.add( Integer.toString(R.drawable.terapia_anomalia));
				     }
					 else if(fotoListTherapy.get(0).getFoto().equals("1"))
					 {
						fotoList.add( Integer.toString(R.drawable.terapie_1 ));
					 }	 
					 else if(fotoListTherapy.get(0).getFoto().equals("2"))
					 {
						fotoList.add( Integer.toString(R.drawable.terapie_completata));
					 }	
		    }
			if (fotoList != null)
			{
				singleDay.setFotoList(fotoList);
			}
			eachDay.add(0, singleDay);
			calendarForward.add(Calendar.DATE, 1);
		}
	    dailyElementsOperations.close();
		listTexts = new ArrayList<List>((Collection<? extends List>) eachDay);
		
		try {
			myAdapter = new EffeciListAdapter(calendaractual.get(Calendar.YEAR)+ "-" +(calendaractual.get(Calendar.MONTH)+1) + "-" + calendaractual.get(Calendar.DATE), getActivity(), listTexts, 3);
			lvMenu.setAdapter(myAdapter);
			myAdapter.notifyDataSetChanged();
			lvMenu.setSelection(30);
		}
		catch(Exception e)
		{
			Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}
}
