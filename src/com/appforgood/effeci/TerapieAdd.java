package com.appforgood.effeci;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import com.appforgood.effeciObjectModels.*;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TerapieAdd extends Fragment implements OnFocusChangeListener, OnClickListener 
{
	LinearLayout llFooter;
	RelativeLayout timeContainer;
	EditText  etTherapyName, etNote, etOrari , etDurataGiorni, etQuantiGiorni;
	Fragment fragment = null;
	public static TextView etDate,tvStartTime, tvEndTime, tvPlusDurataGiorni, tvMinusDurataGiorni, 
	tvPlusQuantiGiorni,tvMinusQuantiGiorni;
	RelativeLayout btnBack;
	ImageButton  btnEditOrari,btnEditDate ;
	ImageView btnSaveTerapie;
	ArrayList<BacteriumObject> bacteriumList;
	public static String selectedDate;
	CharSequence[] timeListChar ;
	SharedPreferences preference;
	int plusDuratagiorni,minusDurataGiorni, plusQuantiGiorni,minusQuantiGiorni;
	long idTherapy;
	long duration;
	int frequency;
	String startDate;
	String dateDb;
	long rowTime;
	Calendar calendar;
	Editor editor;
	String typeCharDuartion = "1";
	String typeCharFrequency = "1";
	String  regex = "\\d*";
	final boolean[] timeChecked = new boolean[48];
	
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
	{
		preference = this.getActivity().getSharedPreferences("SharedPreferencesEffeciTimeCheckBox", Context.MODE_PRIVATE);
		editor = preference.edit();
		timeListChar = new CharSequence[48];

		for(int i = 0 ; i<24; i++)
		{
			String firstTime;
			String secondTime;
			if(i < 10) 
			{
				firstTime = "0"+String.valueOf(i)+":00";
				secondTime = "0"+String.valueOf(i)+":30";
			}
			else
			{
				firstTime = String.valueOf(i)+":00";
				secondTime = String.valueOf(i)+":30";
			}
			timeListChar[i*2] = firstTime;
			timeListChar[i*2 +1] = secondTime;
		}
		for(int i = 0; i < timeListChar.length; i++) 
		{
			editor.putBoolean(String.valueOf(timeListChar[i]), false);
		}
		editor.commit();
		
		View actualView = inflater.inflate(R.layout.terapie_add, container, false);
		ActionBar actionBar = getActivity().getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater inflaterLayout= (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View customView = inflaterLayout.inflate(R.layout.terapie_add_actionbar, null);
		
		btnBack = (RelativeLayout) customView.findViewById(R.id.back);
		timeContainer = (RelativeLayout) customView.findViewById(R.id.timeContainer);
		btnSaveTerapie = (ImageView) customView.findViewById(R.id.btnSaveTerapie);
		btnEditOrari = (ImageButton) actualView.findViewById(R.id.btnEditOrari);
		btnEditDate = (ImageButton) actualView.findViewById(R.id.btnEditDate);
		btnBack.setOnClickListener(this);
		btnSaveTerapie.setOnClickListener(this);
		btnEditOrari.setOnClickListener(this);
		btnEditDate.setOnClickListener(this);
		
		actionBar.setCustomView(customView);
		actionBar.setDisplayShowCustomEnabled(true);
		
		llFooter = (LinearLayout) getActivity().findViewById(R.id.llHomeFooter);
		etTherapyName = (EditText) actualView.findViewById(R.id.etNomeTherapy);
		etNote = (EditText) actualView.findViewById(R.id.etNote);
		etDate = (TextView) actualView.findViewById(R.id.tvDate);
		Calendar calendar = Calendar.getInstance(Locale.ITALY);
		selectedDate = (calendar.get(Calendar.YEAR)) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE);
		etDate.setText((calendar.get(Calendar.MONTH)+1) + "/" + calendar.get(Calendar.DATE) + "/" + calendar.get(Calendar.YEAR));
		etDurataGiorni = (EditText) actualView.findViewById(R.id.etDurataGiorni);
		etQuantiGiorni = (EditText) actualView.findViewById(R.id.etQuantiGiorni);
		tvPlusDurataGiorni= (TextView) actualView.findViewById(R.id.btnPlusDurataGiorni);
		tvPlusDurataGiorni.setOnClickListener(this);
		tvMinusDurataGiorni= (TextView) actualView.findViewById(R.id.btnMinusDurataGiorni);
		tvMinusDurataGiorni.setOnClickListener(this);
		tvPlusQuantiGiorni= (TextView) actualView.findViewById(R.id.btnPlusQuantiGiorni);
		tvPlusQuantiGiorni.setOnClickListener(this);
		tvMinusQuantiGiorni= (TextView) actualView.findViewById(R.id.btnMinusQuantiGiorni);
		tvMinusQuantiGiorni.setOnClickListener(this);
		etNote.setOnFocusChangeListener(this);
		etDurataGiorni.setOnFocusChangeListener(this);
		etQuantiGiorni.setOnFocusChangeListener(this);
		
		actualView.setFocusableInTouchMode(true);
		actualView.requestFocus();
		return actualView;
	}
	@Override
	public void onClick(View view)  
	{  
		Intent intent;
		switch (view.getId()) 
		{
			case R.id.back:
				fragment = new Terapie();
				if(fragment != null)
			     {
					FragmentManager fragmentManager = getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.flHome, fragment).addToBackStack("");
					fragmentTransaction.commit();
			     }
		     break;
		     
		      case R.id.btnPlusDurataGiorni:
	    	    typeCharDuartion = etDurataGiorni.getText().toString().replaceAll("\\s+","");
		      	if((typeCharDuartion.matches(regex)) && (!typeCharDuartion.matches("")))
		      	{ plusDuratagiorni=Integer.parseInt(etDurataGiorni.getText().toString());
				 plusDuratagiorni++;
				 etDurataGiorni.setText(Integer.toString(plusDuratagiorni));
				}
		      	else
		      	{
					 Toast.makeText(getActivity(), "Vendosni nje vlere numerike per Kohezgjatjen ne dite!", Toast.LENGTH_LONG).show();
		      	}
				 break;
				 
			 case R.id.btnPlusQuantiGiorni:
				typeCharFrequency = etQuantiGiorni.getText().toString().replaceAll("\\s+","");
		      	if((typeCharFrequency.matches(regex)) && (!typeCharFrequency.matches("")))
				 {plusQuantiGiorni=Integer.parseInt(etQuantiGiorni.getText().toString());
				 plusQuantiGiorni++;
				 etQuantiGiorni.setText(Integer.toString(plusQuantiGiorni));
				 }
		      	else
		      	{
		      		Toast.makeText(getActivity(), "Vendosni nje vlere numerike per Sa here ne dite!", Toast.LENGTH_LONG).show();
		      	}
				 break;
				 
			 case R.id.btnMinusDurataGiorni:
				 typeCharDuartion = etDurataGiorni.getText().toString().replaceAll("\\s+","");
		      	 if((typeCharDuartion.matches(regex)) && (!typeCharDuartion.matches("")))
		      	 {
		      		 minusDurataGiorni=Integer.parseInt(typeCharDuartion);
		      		 if (minusDurataGiorni==1)
		      		 { 
		      			 etDurataGiorni.setText(Integer.toString(1));
		      		 }
					else
					{
						minusDurataGiorni--;
						etDurataGiorni.setText(Integer.toString(minusDurataGiorni));
					}
		      	 }
		      	 else
		      	 {
					 Toast.makeText(getActivity(), "Vendosni nje vlere numerike per Kohezgjatjen ne dite!", Toast.LENGTH_LONG).show();
		      	 }
				 break;
				 
			 case R.id.btnMinusQuantiGiorni:
				 typeCharFrequency = etQuantiGiorni.getText().toString().replaceAll("\\s+","");
		      	 if((typeCharFrequency.matches(regex)) && (!typeCharFrequency.matches("")))
				 {
		      		 minusQuantiGiorni=Integer.parseInt(typeCharFrequency);
		      		 if (minusQuantiGiorni==1)
		      		 { etQuantiGiorni.setText(Integer.toString(1));
		      		 }
		      		 else
		      		 {
						 minusQuantiGiorni--;
						 etQuantiGiorni.setText(Integer.toString(minusQuantiGiorni));
		      		 }
				 }
		      	else
		      	{
		      		Toast.makeText(getActivity(), "Vendosni nje vlere numerike per Sa here ne dite!", Toast.LENGTH_LONG).show();
		      	}
		      	 
				 break;
			 case R.id.btnSaveTerapie: 
				 if(!etTherapyName.getText().toString().matches(""))
				 {
					 String drurataGiorni = etDurataGiorni.getText().toString().replaceAll("\\s+","");
					 String frequencyGiorni = etQuantiGiorni.getText().toString().replaceAll("\\s+","");
					 
					 if((drurataGiorni.matches(regex)) && (!drurataGiorni.matches("")) && (frequencyGiorni.matches(regex)) && (!frequencyGiorni.matches("")))
					 {
						 if(TimeDialogCheckBox.flagTime)
						 {
						     TherapyObject therapyObject = new TherapyObject();
							 therapyObject.setTherapyName(etTherapyName.getText().toString());
							 therapyObject.setNotes(etNote.getText().toString());
							 therapyObject.setDuration(Integer.parseInt(drurataGiorni));
							 therapyObject.setFrequency(Integer.parseInt(frequencyGiorni));
							 therapyObject.setStartDate(selectedDate);
							 therapyObject.setStatus(1);
							 TherapyOperations therapyOperations = new TherapyOperations(getActivity());
							 long row = therapyOperations.insertTherapy(therapyObject);
						 
						 if (row < 0)
						 {
							 Toast.makeText(getActivity(), "Ka ndodhur nje gabim gjate shtimit te terapise se re!", Toast.LENGTH_LONG).show();
						 }
						 else 
						 {
							 Toast.makeText(getActivity(), "Terapia eshte shtuar me sukses!", Toast.LENGTH_SHORT).show();
							 idTherapy = therapyOperations.getLastTherapyId();
							 duration = therapyObject.getDuration();
							 frequency = therapyObject.getFrequency();
							 startDate = therapyObject.getStartDate();
							 onClick(btnBack);
						 }
					 
					 for(int i=0; i<48; i++)
						{
							timeChecked[i] = preference.getBoolean(String.valueOf(timeListChar[i]), false);
						}
						editor.commit();
						
					TimeTherapyObject timeObject = new TimeTherapyObject();
					TimeTherapyOperations timeTherapyOperations = new TimeTherapyOperations(getActivity());
					
					calendar = Calendar.getInstance(Locale.ITALY);
					for (int j = 0; j< duration/frequency; j++)
					 {
						 int addDate= j*frequency;
						 SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" ); 
						 try 
						 {
							calendar.setTime( dateFormat.parse(startDate));
						} 
						 catch (ParseException e)
						 {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						 calendar.add(Calendar.DATE, addDate);
						 dateDb = (calendar.get(Calendar.YEAR)) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE);
					 for(int i=0; i<48; i++)
						{
							if(timeChecked[i] == true)
							{
								timeObject.setTherapyId(idTherapy);
								timeObject.setTime(timeListChar[i].toString());
								timeObject.setDateId(dateDb);
								timeObject.setStatus(2);
								rowTime = timeTherapyOperations.insertTime(timeObject);
							}
						}
					 }
						 if (rowTime < 0)
						 {
							 Toast.makeText(getActivity(), "Ka ndodhur nje gabim gjate shtimit te kohezgjatjes se terapise!", Toast.LENGTH_LONG).show();
						 }
						 else
						 {
							 onClick(btnBack);
						 }
						 TimeDialogCheckBox.flagTime = false;
				 }
				 else 
				 {
					 Toast.makeText(getActivity(), "Vendosni orarin e marrjes se medikamenteve", Toast.LENGTH_LONG).show();
				 }
				 }
				 else 
				 {
					 Toast.makeText(getActivity(), "Vendosni vlera numerike!", Toast.LENGTH_LONG).show();
				 }
					 View closeWindow = this.getActivity().getCurrentFocus();
					 if (closeWindow != null) {  
					     InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					     imm.hideSoftInputFromWindow(closeWindow.getWindowToken(), 0);
					 }
				 }
				 else
				 {
					 Toast.makeText(getActivity(), "Vendosni emrin e terapise!", Toast.LENGTH_LONG).show();
				 }
			 break;	
			 
			 case R.id.btnEditOrari: 
				 FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
		 		 TimeDialogCheckBox dialog = TimeDialogCheckBox.newInstance();
		 		 dialog.show(ft, "Tag");
		 		 break;
		 		 
			 case R.id.btnEditDate:
				 intent = new Intent(getActivity(), DateTimePickerActivity.class);
				 intent.putExtra("picker_type", "date");
				 startActivity(intent);
				 break;
	 }
	}

	@Override
	public void onFocusChange(View view, boolean argument) 
	{
		if(view.getId() == R.id.etNomeTherapy || view.getId() == R.id.etNote || view.getId() == R.id.etDurataGiorni || view.getId() == R.id.tvDate || view.getId() == R.id.timeContainer|| view.getId() == R.id.etQuantiGiorni ) 
		{
			if (argument) 
			{
				llFooter.setVisibility(View.VISIBLE);
			}
		}
		else 
		{
			Toast.makeText(getActivity(), " U shtyp butoni kthehu", Toast.LENGTH_LONG).show();
			llFooter.setVisibility(View.VISIBLE);
		}
	}
}


