package com.appforgood.effeci;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DatiSalute extends Fragment implements OnClickListener
{
	@Override
	   public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
	   {
			View thisActualView = inflater.inflate(R.layout.dati_salute, container, false);
			ActionBar thisActionBar = getActivity().getActionBar();
			thisActionBar.setDisplayShowHomeEnabled(false);
			thisActionBar.setDisplayShowTitleEnabled(false);
			LayoutInflater varInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View thisCustomView = varInflater.inflate(R.layout.dati_salute_actionbar, null);
			thisActionBar.setCustomView(thisCustomView);
			thisActionBar.setDisplayShowCustomEnabled(true);
			Button btnConcediAccessoAiDatiSaluto, btnDisattivaAccessoAiDatiDiSaluto;
			btnConcediAccessoAiDatiSaluto = (Button) thisActualView.findViewById(R.id.btnConcediAccessoAiDatiSaluto);
			btnDisattivaAccessoAiDatiDiSaluto = (Button) thisActualView.findViewById(R.id.btnDisattivaAccessoAiDatiDiSaluto);
			btnConcediAccessoAiDatiSaluto.setOnClickListener(this);
			btnDisattivaAccessoAiDatiDiSaluto.setOnClickListener(this);
			
			return thisActualView;
	   }  
	
	@Override
	public void onClick(View view) 
	{
		switch (view.getId()) 
		{
			case R.id.btnConcediAccessoAiDatiSaluto:
				Fragment notificheOne = new Notifiche();
				if(notificheOne != null)
		        {
					SharedPreferences pref = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
					Editor editor = pref.edit();
					editor.putBoolean("dati_salute", true);
					editor.commit();
					
					FragmentManager fragmentManager = getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.flHome, notificheOne);
					fragmentTransaction.commit();
		        }
				break;
			case R.id.btnDisattivaAccessoAiDatiDiSaluto:
				Fragment notificheTwo = new Notifiche();
				if(notificheTwo != null)
		        {
					SharedPreferences pref = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
					Editor editor = pref.edit();
					editor.putBoolean("dati_salute", false);
					editor.commit();
					
					FragmentManager fragmentManager = getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.flHome, notificheTwo);
					fragmentTransaction.commit();
		        }
				break;
		}
	}
}

