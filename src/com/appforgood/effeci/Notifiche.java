package com.appforgood.effeci;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Notifiche extends Fragment implements OnClickListener
{
	@Override
	   public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
	   {
		View actualView = inflater.inflate(R.layout.notifiche, container, false);
		
			ActionBar actionBar = getActivity().getActionBar();
			actionBar.setDisplayShowHomeEnabled(false);
			actionBar.setDisplayShowTitleEnabled(false);
			LayoutInflater inflaterLayout = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View customView = inflaterLayout.inflate(R.layout.notifiche_actionbar, null);
			actionBar.setCustomView(customView);
			actionBar.setDisplayShowCustomEnabled(true);
			Button btnConcediInvioNotifiche, btnDisattivaInvioNotifiche;
			btnConcediInvioNotifiche = (Button) actualView.findViewById(R.id.btnConcediInvioNotifiche);
			btnDisattivaInvioNotifiche = (Button) actualView.findViewById(R.id.btnDisattivaInvioNotifiche);
			btnConcediInvioNotifiche.setOnClickListener(this);
			btnDisattivaInvioNotifiche.setOnClickListener(this);
			
			return actualView;
	   }  
	
	@Override
	public void onClick(View view) 
	{
		switch (view.getId()) 
		{
			case R.id.btnConcediInvioNotifiche:
				Fragment diarioOne = new Diario();
				if(diarioOne != null)
		        {
					SharedPreferences preference = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
					Editor editor = preference.edit();
					editor.putBoolean("notifiche", true);
					editor.commit();
					
					FragmentManager fragmentManager = getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.flHome, diarioOne);
					fragmentTransaction.commit();
		        }
				break;
			case R.id.btnDisattivaInvioNotifiche:
				Fragment diarioTwo = new Diario();
				if(diarioTwo != null)
		        {
					SharedPreferences preference = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
					Editor editor = preference.edit();
					editor.putBoolean("notifiche", false);
					editor.commit();
					
					FragmentManager fragmentManager = getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.flHome, diarioTwo);
					fragmentTransaction.commit();
		        }
				break;
			}
	}
}

