package com.appforgood.effeci;

import java.util.ArrayList;
import java.util.List;

import com.appforgood.effeciObjectModels.TherapyObject;
import com.appforgood.effeciObjectModels.TherapyOperations;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Terapie extends Fragment implements OnClickListener 
{
	ListView lvMenu;
	public static int positionTherapy ;
	ArrayList<List> therapyList;
	Button btnAdd;
	
	@Override
	   public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
	   {
			View thisActualView = inflater.inflate(R.layout.terapie, container, false);
			
			ActionBar thisActionBar = getActivity().getActionBar();
			thisActionBar.setDisplayShowHomeEnabled(false);
			thisActionBar.setDisplayShowTitleEnabled(false);
			LayoutInflater varInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View thisCustomView = varInflater.inflate(R.layout.terapie_actionbar, null);
			thisActionBar.setCustomView(thisCustomView);
			thisActionBar.setDisplayShowCustomEnabled(true);
			btnAdd = (Button) getActivity().findViewById(R.id.bTerapieAdd);
			
			TherapyOperations varTherapyOperations = new TherapyOperations(getActivity());
			therapyList = varTherapyOperations.getTherapies();
			String[] listTexts1 = new String[therapyList.size()];
			
			btnAdd.setOnClickListener(this);
			try 
			{
				lvMenu = (ListView) thisActualView.findViewById(R.id.lvTerapieList);
				EffeciListAdapter myAdapter = new EffeciListAdapter("",getActivity(), therapyList, 2);
				lvMenu.setAdapter(myAdapter);
			}
			catch(Exception e) 
			{
				Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
			}
			lvMenu.setOnItemClickListener(new OnItemClickListener() 
			{
		          public void onItemClick(AdapterView<?> parent, final View view,
		              int position, long id) 
		          { 
		        	  view.setBackgroundColor(Color.parseColor("#e6e6e6"));
					  final Handler handler = new Handler();
					  handler.postDelayed(new Runnable() 
						{
						  @Override
						  public void run() 
						  {
							  view.setBackgroundColor(Color.parseColor("#ffffff"));
						  }
						}, 200);
		        	  int itemId = EffeciListAdapter.getIdItem(position);
			          TherapyOperations therapy= new TherapyOperations(getActivity());
			          TherapyObject objectStatus= therapy.getTherapyByID(itemId);
			          objectStatus.getStatus();
		        	 
		        	 if(objectStatus.getStatus()!=2)
		        	 {
		        	  Fragment terapieElement = null;
		        	  terapieElement = new TerapieEdit(itemId);
		              if(terapieElement != null)
		              {
		    			FragmentManager fragmentManager = getFragmentManager();
		    			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		    			fragmentTransaction.replace(R.id.flHome, terapieElement).addToBackStack("");;
		    			fragmentTransaction.commit();
		              }
		        	 } 
		}});
			SharedPreferences pref = getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
			boolean notificheShPreferences = pref.getBoolean("notifiche", false);
			if (notificheShPreferences)
			{
				Home.initializeNotification(notificheShPreferences, getActivity());
			}
			return thisActualView;
	   }  
	
	@Override
	public void onClick(View view) 
	{
		Fragment fragment = null;
		
        if (view.getId() == R.id.bTerapieAdd) 
        {
        	fragment = new TerapieAdd();
        }
        if(fragment != null)
        {
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.flHome, fragment).addToBackStack("");
			fragmentTransaction.commit();
        }
	}
}
