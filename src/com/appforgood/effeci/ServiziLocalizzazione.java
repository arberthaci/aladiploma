package com.appforgood.effeci;

import com.appforgood.effeci.Profilio.changeDeviceName;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.view.ViewGroup;

public class ServiziLocalizzazione extends Fragment implements OnClickListener
{
    SharedPreferences prefBacterium = null;
    Editor editorBacterium = null;

    @Override
       public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
       {
            prefBacterium = this.getActivity().getSharedPreferences("SharedPreferencesEffeciCheckBox", Context.MODE_PRIVATE);
            editorBacterium = prefBacterium.edit();

            View varActualView = inflater.inflate(R.layout.servizi_localizzazione, container, false);

            ActionBar varActionBar = getActivity().getActionBar();
            varActionBar.setDisplayShowHomeEnabled(false);
            varActionBar.setDisplayShowTitleEnabled(false);
            LayoutInflater varInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View varCustomView = varInflater.inflate(R.layout.servizi_localizzazione_actionbar, null);

            varActionBar.setCustomView(varCustomView);
            varActionBar.setDisplayShowCustomEnabled(true);
            Button btnAttivaServizi, btnDisattivaRilevamentoBatteri;

            btnAttivaServizi = (Button) varActualView.findViewById(R.id.btnAttivaServizi);
            btnDisattivaRilevamentoBatteri = (Button) varActualView.findViewById(R.id.btnDisattivaRilevamentoBatteri);

            btnAttivaServizi.setOnClickListener(this);
            btnDisattivaRilevamentoBatteri.setOnClickListener(this);

            return varActualView;
       }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
        case R.id.btnAttivaServizi:
            {
                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter == null)
                {
                    Toast.makeText(getActivity(), "Kjo pajisje nuk suporton bluetooth-in", Toast.LENGTH_LONG).show();
                }
                else
                {
                if (!mBluetoothAdapter.isEnabled())
                {
                    mBluetoothAdapter.enable();
                    editorBacterium.putBoolean("0", true);
                    Toast.makeText(getActivity(), "Zbulimi i pajisjeve me te njejtin aplikacion dhe te njejten semundje eshte aktivizuar", Toast.LENGTH_LONG).show();

                    Fragment datiSaluteOne = new DatiSalute();
                    if(datiSaluteOne != null)
                    {
                        SharedPreferences preference = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
                        Editor editor = preference.edit();
                        editor.putBoolean("attiva_servizzi", true);
                        editor.commit();

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.flHome, datiSaluteOne);
                        fragmentTransaction.commit();
                    }
                }
                else
                {
                    Fragment datiSaluteThree = new DatiSalute();
                    if(datiSaluteThree != null)
                    {
                        SharedPreferences pref = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
                        Editor editor = pref.edit();
                        editor.putBoolean("attiva_servizzi", true);
                        editor.commit();

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.flHome, datiSaluteThree);
                        fragmentTransaction.commit();
                    }
                }
                }
                break;
            }

        case R.id.btnDisattivaRilevamentoBatteri:
        {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null)
            {
                Toast.makeText(getActivity(), "Kjo pajisje nuk suporton bluetooth-in", Toast.LENGTH_LONG).show();
            }
            else
            {
                if (mBluetoothAdapter.isEnabled())
                {
                    Toast.makeText(getActivity(), "Zbulimi i pajisjeve me te njejtin aplikacion dhe te njejten semundje eshte šaktivizuar", Toast.LENGTH_LONG).show();
                    Fragment datiSaluteTwo = new DatiSalute();
                    if(datiSaluteTwo != null)
                    {
                        SharedPreferences pref = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
                        Editor editor = pref.edit();
                        editor.putBoolean("attiva_servizzi", false);
                        editor.commit();

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.flHome, datiSaluteTwo);
                        fragmentTransaction.commit();
                    }
                }
                else
                {
                    Fragment datiSaluteFour = new DatiSalute();
                    if(datiSaluteFour != null)
                    {
                        SharedPreferences pref = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
                        Editor editor = pref.edit();
                        editor.putBoolean("attiva_servizzi", false);
                        editor.commit();

                        FragmentManager varFragmentManager = getFragmentManager();
                        FragmentTransaction varFragmentTransaction = varFragmentManager.beginTransaction();
                        varFragmentTransaction.replace(R.id.flHome, datiSaluteFour);
                        varFragmentTransaction.commit();
                    }
                }
                break;
            }
        }
      }
    }
}