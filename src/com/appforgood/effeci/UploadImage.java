package com.appforgood.effeci;

import java.io.ByteArrayOutputStream;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.media.ThumbnailUtils;

public class UploadImage extends Activity implements OnClickListener
{ 
	    private static int RESULT_LOAD_IMAGE = 1;
	    SharedPreferences preference;
	    EditText textName, textSurName;
	    Button buttonSave;
	    ImageView buttonLoadImage;
	    private final double WIDTH = 720.0;
	    private final double HEIGHT = 1280.0;
	    RelativeLayout btnBack;
	    Bitmap bitmap;
	    RoundImage roundedImage ;
	    
	    @Override
	    public void onCreate(Bundle savedInstanceState) 
	    {
	        super.onCreate(savedInstanceState);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.image_upload);
	        preference = getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
	        
	        ActionBar thisActionBar = getActionBar();
			thisActionBar.setDisplayShowHomeEnabled(false);
			thisActionBar.setDisplayShowTitleEnabled(false);
			LayoutInflater varInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			View thisCustomView = varInflater.inflate(R.layout.upload_image_actionbar, null);
			thisActionBar.setCustomView(thisCustomView);
			thisActionBar.setDisplayShowCustomEnabled(true);
			
			btnBack = (RelativeLayout) thisCustomView.findViewById(R.id.backButtonUploadImage);
			btnBack.setOnClickListener(this);
	        
	        buttonLoadImage = (ImageView) findViewById(R.id.getImageButton);
	        buttonSave = (Button) findViewById(R.id.btnSaveProfilo);
	        textName = (EditText) findViewById(R.id.name);
	        textSurName = (EditText) findViewById(R.id.cogname);
	        textName.setText(preference.getString("myName", "Emri"));
	        textSurName.setText(preference.getString("mySurname", "Mbiemri"));
	        buttonSave.setOnClickListener(this);
	        buttonLoadImage.setOnClickListener(new View.OnClickListener() {
	             
	            @Override
	            public void onClick(View view) 
	            {
	                Intent i = new Intent(
	                Intent.ACTION_PICK,
	                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	                startActivityForResult(i, RESULT_LOAD_IMAGE);
	            }
	        });
	    }
	     
	    @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	    {
	        super.onActivityResult(requestCode, resultCode, data);
	         
	        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) 
	        {
	            Uri selectedImage = data.getData();
	            String[] filePathColumn = { MediaStore.Images.Media.DATA };
	 
	            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
	            cursor.moveToFirst();
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            String picturePath = cursor.getString(columnIndex);
	            cursor.close();
	            /*//ImageView imageView = (ImageView) findViewById(R.id.getImageView);*/
	            bitmap = BitmapFactory.decodeFile(picturePath);
	            if(bitmap.getWidth() > WIDTH && bitmap.getHeight() > HEIGHT)
	            {
	            	 bitmap = ThumbnailUtils.extractThumbnail(bitmap, 100, 100);
	            	//bitmap = ThumbnailUtils.extractThumbnail(bitmap, bitmap.getWidth()/4, bitmap.getHeight()/4);
	            	//buttonLoadImage.setImageBitmap(bitmap);
	            	roundedImage = new RoundImage(bitmap);
	            	buttonLoadImage.setImageDrawable(roundedImage);
	            }
	            else
	            {
	            	bitmap = ThumbnailUtils.extractThumbnail(bitmap, 100, 100);
	            	roundedImage = new RoundImage(bitmap);
	            	buttonLoadImage.setImageDrawable(roundedImage);
	            	//buttonLoadImage.setImageBitmap(bitmap);
	            }
	            Editor editor = preference.edit();
	            editor.putString("imagePreference", picturePath);
	            editor.putString("myName", textName.getText().toString());
	            editor.commit();
	        }
	    }
        public final String encodeTobase64(Bitmap image)
        {
            Bitmap immage = image;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] b = baos.toByteArray();
            String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

            Log.d("Image Log:", imageEncoded);
            return imageEncoded;
        }

		@Override
		public void onClick(View view) 
		{
			switch(view.getId())
			{
				case R.id.btnSaveProfilo:
				{
					Profilio.isOnResume = true;
					Editor editor = preference.edit();
			        editor.putString("myName", textName.getText().toString());
			        editor.putString("mySurname", textSurName.getText().toString());
			        editor.commit();
					finish();
					break;
				}
				case R.id.backButtonUploadImage:
				{
					finish();
					break;
				}
}}}
	
