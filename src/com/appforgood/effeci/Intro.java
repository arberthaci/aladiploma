package com.appforgood.effeci;

import com.appforgood.effeci.R;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Intro extends Activity 
{
	Fragment test = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 setContentView(R.layout.intro);
		
		Thread timer = new Thread()
		{
            public void run()
            {
                try 
                {
                    sleep(3000);
                } 
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                } 
                finally 
                {
                  Intent diarioActivity = new Intent("com.appforgood.effeci.Home");
                    startActivity(diarioActivity);
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onPause() 
    {
        super.onPause();
        finish();
    }
}
