package com.appforgood.effeci;

import java.util.ArrayList;

import com.appforgood.effeci.Profilio.changeDeviceName;
import com.appforgood.effeciObjectModels.*;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
 
public class DialogBatterieCheckBox extends DialogFragment
{
	ArrayList batterioSelectedItems;
	String pick_batterie = "Zgjidh semundjen";
	LinearLayout batterioContainer;
	ArrayList<BacteriumObject> bacteriumArrayList = new ArrayList<BacteriumObject>();

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) 
	{
		BacteriumOperations bacteriumOperations = new BacteriumOperations(getActivity());
		bacteriumArrayList = bacteriumOperations.getAllBacteriums();
		final CharSequence[] batterioList = new String[bacteriumArrayList.size()];
		final int[] bacteriumIdList = new int[bacteriumArrayList.size()];
		final boolean[] batterioChecked =  new boolean[bacteriumArrayList.size()];
		
		for (int i =0 ; i < bacteriumArrayList.size(); i++)  
		{ 
			batterioList[i] = bacteriumArrayList.get(i).getBacteriumName();
			bacteriumIdList[i] = bacteriumArrayList.get(i).getBacteriumId();
		}
		
		SharedPreferences pref = this.getActivity().getSharedPreferences("SharedPreferencesEffeciCheckBox", Context.MODE_PRIVATE);
		final Editor editor = pref.edit();
		for (int i =0 ; i < bacteriumArrayList.size(); i++)
		{
			batterioChecked[i] = pref.getBoolean(String.valueOf(bacteriumIdList[i]), false);
		}
		
		batterioSelectedItems = new ArrayList();  // Where we track the selected items
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Set the dialog title
		builder.setTitle(pick_batterie)
		// Specify the list array, the items to be selected by default (null for none),
		// and the listener through which to receive callbacks when items are selected
		.setMultiChoiceItems(batterioList, batterioChecked,
				new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which,
					boolean isChecked) {
				if (isChecked) {
					// If the user checked the item, add it to the selected items
					batterioSelectedItems.add(which);
					batterioChecked[which] = true;
				}
				else if(!isChecked){
					batterioChecked[which] = false;
				} 
			}
		}
				)
		
		// Set the action buttons
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				// User clicked OK, so save the mSelectedItems results somewhere
				// or return them to the component that opened the dialog
				
				batterioContainer = (LinearLayout) getActivity().findViewById(R.id.batterioContainer);
				batterioContainer.removeAllViews();
				int j = 0;
				for (int i = 0; i<batterioChecked.length; i++)
				{
					if(batterioChecked[i]== true )
					{
						if(j<1)
						{
							TextView textView = new TextView(getActivity());
							LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
							        		  LayoutParams.WRAP_CONTENT);
						    params.setMargins(0, 0, 10, 0); 
						    textView.setLayoutParams(params);
						    textView.setText(batterioList[i].toString());
						    textView.setTextSize(18);
						    textView.setTextColor(Color.parseColor("#a3a3a3"));
						    textView.setTypeface(null, Typeface.BOLD);
						    batterioContainer.addView(textView);
						}
						 j++;
					}
				}
				if(j>1)
				{
					TextView textView = new TextView(getActivity());
			        LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		            params.setMargins(0, 0, 10, 0); 
		            textView.setLayoutParams(params);
		            textView.setText("...");
			        textView.setTextColor(Color.parseColor("#636363"));
		            textView.setTypeface(null, Typeface.BOLD);
		            batterioContainer.addView(textView);
				}
				
				for (int i =0 ; i < bacteriumArrayList.size(); i++)
				{
					editor.putBoolean(String.valueOf(bacteriumIdList[i]), batterioChecked[i]);
				}
				editor.commit();

				BacteriumOperations bacteriumOperations = new BacteriumOperations(getActivity());

				for (int i =0 ; i < bacteriumArrayList.size(); i++)
				{
					 bacteriumOperations.updateBacteriumStatus(batterioList[i].toString(), batterioChecked[i]);
				}
			}
		})
		.setNegativeButton("Anullo", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		return builder.create();
	}
	
	
	
	public static DialogBatterieCheckBox newInstance() {
		DialogBatterieCheckBox f = new DialogBatterieCheckBox();
		//changeDeviceName();
		return f;
	}
}