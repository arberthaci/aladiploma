package com.appforgood.effeci;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.appforgood.effeciObjectModels.CalendarObject;
import com.appforgood.effeciObjectModels.DailyCheckUp;
import com.appforgood.effeciObjectModels.DailyCheckUpOperations;
import com.appforgood.effeciObjectModels.DiarioObject;
import com.appforgood.effeciObjectModels.TimeTherapyObject;
import com.appforgood.effeciObjectModels.TimeTherapyOperations;

import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EffeciListAdapter extends BaseAdapter implements OnClickListener
{
	private Context thisContext;
	private static ArrayList<List> list;
	private int listViewId;
	public int position;
	public String date;
	private ArrayList<Integer> imageMapped;
	final CharSequence[] statusList ={"E kam marre", "Nuk e kam marre", "Nuk e mbaj mend"};
	boolean[] statusChecked = new boolean[statusList.length] ;
	ArrayList<TimeTherapyObject> timeArrayList = new ArrayList<TimeTherapyObject>();
	Editor editor = null;
	ArrayList timeSelectedItems;
	SharedPreferences pref = null;
	AlertDialog levelDialog;
	int checkedValue = -1;
	int timeId = -1;
	int frequency = 500;
	
	public EffeciListAdapter(String date,Context context, ArrayList<List> list, int listViewId) 
	{
		thisContext = context;
		this.list = list;
		this.listViewId = listViewId;
		this.date = date;
		if (listViewId == 3) 
		{
			imageMapped = new ArrayList<Integer>();
			imageMapped.add(R.id.ivCalendarioListIconOne);
			imageMapped.add(R.id.ivCalendarioListIconTwo);
			imageMapped.add(R.id.ivCalendarioListIconThree);
			imageMapped.add(R.id.ivCalendarioListIconFour);
			imageMapped.add(R.id.ivCalendarioListIconFive);
			imageMapped.add(R.id.ivCalendarioListIconSix);
		}
	}
	
    @Override  
    public int getCount() 
    {
        if(list != null && list.size() != 0)
        {
            return list.size();
        }
        return 0;
    }

    @Override
    public  Object getItem(int position) 
    {
        return list.get(position);
    }
   
    @Override
    public long getItemId(int position) 
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) 
    { 
    	pref = thisContext.getSharedPreferences("SharedPreferencesEffeciStatusCheckBox", Context.MODE_PRIVATE);
		editor = pref.edit();
		
    	switch(listViewId)
    	{ 
    		case 1:
    				final DiarioViewHolder thisDiarioHolder;
    	        	thisDiarioHolder = new DiarioViewHolder();
    	            LayoutInflater inflaterDiario = (LayoutInflater) thisContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	            convertView = inflaterDiario.inflate(R.layout.diario_listview_single_item, null);
    	            thisDiarioHolder.ivListIcon = (ImageView) convertView.findViewById(R.id.ivDiarioListIcon);
    	            thisDiarioHolder.tvListText = (TextView) convertView.findViewById(R.id.tvDiarioListText);
    	            thisDiarioHolder.tvExtraInfo = (TextView) convertView.findViewById(R.id.tvDiarioExtrInfo);
	    	        final DiarioObject listObject = (DiarioObject) list.get(position);
	    	        thisDiarioHolder.ref = position;
	    	        if(position < 6)
	    	        {
	    	        	thisDiarioHolder.ivListIcon.setImageResource(Integer.parseInt(listObject.getFoto()));
	    	        }
	    	        else
	    	        {
	    	        	if(Integer.parseInt(listObject.getFoto())== 1)
	    	        	{
	    	        		thisDiarioHolder.ivListIcon.setImageResource(R.drawable.terapie_completata);
	    	        	}
	    	        	else if(Integer.parseInt(listObject.getFoto())== 0)
	    	        	{
	    	        		thisDiarioHolder.ivListIcon.setImageResource(R.drawable.interrota);
	    	        	}
	    	        	else 
	    	        	{
	    	        		Calendar actualTime = Calendar.getInstance(Locale.US);
	    	        		if(date.equalsIgnoreCase(actualTime.get(Calendar.YEAR)+ "-" +(actualTime.get(Calendar.MONTH)+1) + "-" + actualTime.get(Calendar.DATE)))
	    	        		{
		    	        		String[] checkTime = listObject.getValue().split(":");
		    	        		Calendar therapyTime  = Calendar.getInstance(Locale.US); 
		    	        		therapyTime.set(Calendar.HOUR, Integer.parseInt(checkTime[0]));
		    	        		therapyTime.set(Calendar.MINUTE, Integer.parseInt(checkTime[1]));
			    	        		
		    	        		if(therapyTime.getTimeInMillis() < actualTime.getTimeInMillis())
		    	        		{
		    	        			thisDiarioHolder.ivListIcon.setImageResource(R.drawable.terapia_anomalia);
		    	        		}
		    	        		else 
		    	        		{
		    	        		    thisDiarioHolder.ivListIcon.setImageResource(R.drawable.terapia_futura);
		    	        	    }
	    	        		}
	    	        		else
	    	        		{
	    	        			try
	    	        			{
		    	        			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		    	        			Date dateChoosen = formatter.parse(date);
		    	        			Date dateActual =  formatter.parse(actualTime.get(Calendar.YEAR)+ "-" +(actualTime.get(Calendar.MONTH)+1) + "-" + actualTime.get(Calendar.DATE));
		    	        			
		    	        			if(dateChoosen.compareTo(dateActual)>0 )
	    	        			{
	    	        				thisDiarioHolder.ivListIcon.setImageResource(R.drawable.terapia_futura);
	    	        			}
	    	        			else
	    	        			{
	    	        				thisDiarioHolder.ivListIcon.setImageResource(R.drawable.terapia_anomalia);
	    	        			}
	    	        			}
	    	        			catch (Exception e1)
	    	        			{
	    	        		        e1.printStackTrace();
	    	        		    }
	    	        		}
	    	             } 
    	        }
    	        thisDiarioHolder.tvListText.setText(listObject.getName());
    	        thisDiarioHolder.tvExtraInfo.setText(listObject.getValue());
    	        //TODO ARBER => FINISHED
    	        /*if(position !=5)
    	        {
    	        	thisDiarioHolder.tvExtraInfo.setText(listObject.getValue());
    	        }
    	        else
    	        { 
    	        	//TODO ARBER
    	        	SharedPreferences pref = thisContext.getSharedPreferences("SharedPreferencesEffeci", thisContext.MODE_PRIVATE);
    	            int actualSteps = pref.getInt("actualSteps", -1);
    	            try {
    	            	thisDiarioHolder.tvExtraInfo.setText(actualSteps);
    	            }
    	            catch(Exception e) {
    	            	Toast.makeText(thisContext, e.toString(), Toast.LENGTH_SHORT).show();
    	            }
    	        	final Handler handler = new Handler();
					handler.postDelayed(new Runnable() 
					{
					  @Override
					  public void run() 
					  {
						thisDiarioHolder.tvExtraInfo.setText(Diario.stepNumber);
						if (thisDiarioHolder.tvExtraInfo.getText().toString().equalsIgnoreCase(""))
  						{
  							handler.postDelayed(this, 500);
  						}
					  }
					}, 750);
    	        }*/
    	        convertView.setOnClickListener(new OnClickListener() 
    	        {
					@Override
					public void onClick(final View view) 
					{
						view.setBackgroundColor(Color.parseColor("#e6e6e6"));
						final Handler handler = new Handler();
						handler.postDelayed(new Runnable() {
						  @Override
						  public void run() {
							  view.setBackgroundColor(Color.parseColor("#ffffff"));
						  }
						}, 200);
						
						final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(thisContext);
						final EditText value = new EditText(thisContext);
						final DailyCheckUpOperations dailyCheckUpOperations = new DailyCheckUpOperations(thisContext);
  						final TimeTherapyOperations timeTherapyOperations = new TimeTherapyOperations(thisContext);
  						final int checkUpId = getCheckUpId(position);
  						
		      			if(position < 5)
		      			{
		      				if(checkUpId > 0)
		      				{
		      					DailyCheckUp checkUpById = dailyCheckUpOperations.getCheckUpById(checkUpId);
		      					value.setText(checkUpById.getValue());
		      				}
		      				if(position != 0)
		      				{
		      				value.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_NUMBER_FLAG_SIGNED);
		      				}
		      				alertDialogBuilder.setTitle("Vendosni vleren");
		      			    alertDialogBuilder.setView(value);
		      			    alertDialogBuilder.setMessage("");
			      			alertDialogBuilder.setCancelable(false);
			      			alertDialogBuilder.setPositiveButton("Ok",new DialogInterface.OnClickListener() 
			      			{
			      					public void onClick(DialogInterface dialog,int id) 
			      					{
			      						if(position<5)
			      						{
					      							if(checkUpId > 0)
				 	      							{
					      								long row = dailyCheckUpOperations.updateDailyCheckUp(checkUpId, value.getText().toString());
					      							}
					      							else
					      							{
					      								DailyCheckUp dailyCheckUpElement = new DailyCheckUp();
					      								dailyCheckUpElement.setIdDailyElements(listObject.getDailyelementid());
					      								dailyCheckUpElement.setDate(date);
					      								dailyCheckUpElement.setValue(value.getText().toString());
					      								long row = dailyCheckUpOperations.insertDailyCheckUp(dailyCheckUpElement);
					      							}
				      						   
			      						}
			      						Diario.updateListView(date);
			      					}
			      				  })
			      				.setNegativeButton("Anullo",new DialogInterface.OnClickListener() 
			      				{
			      					public void onClick(DialogInterface dialog,int id)
			      					{
			      						dialog.cancel();
			      					}
			      				});
				      			AlertDialog alertDialog = alertDialogBuilder.create();
				      			alertDialog.show();
		      			}
		      			else if (position>5)
		      			{
		      				TimeTherapyObject timeObject = timeTherapyOperations.getTimeElementStatusByID(checkUpId);
		      				int status = timeObject.getStatus();
		      				
		                	if(status == 0 )
	  	                		checkedValue = 1;
	  	                	if(status == 1 )
	  	                		checkedValue = 0;
	  	                	if(status == 2 )
	  	                		checkedValue = 2;
	  	                	
		      				final AlertDialog.Builder builder = new AlertDialog.Builder(thisContext);
      	                	builder.setTitle("Verifiko");
		                    builder.setSingleChoiceItems(statusList, checkedValue, new DialogInterface.OnClickListener() 
		                    {
		      	                public void onClick(DialogInterface dialog, int item) 
		      	                {
		      	                        if(statusList[item] == "E kam marre")
		      	                        {
		      	                        	editor.putBoolean("Preso", true);
		      	                        	editor.putBoolean("Non preso", false);
		      	                        	editor.putBoolean("Non Ricordo", false);
		      	                        	editor.commit();
		      	                        }
		      	                        if(statusList[item] == "Nuk e kam marre")
			      	                    {
		      	                        	editor.putBoolean("Preso", false);
		      	                        	editor.putBoolean("Non preso", true);
		      	                        	editor.putBoolean("Non Ricordo", false);
		      	                        	editor.commit();
		      	                        }
			      	                    if(statusList[item] == "Nuk e mbaj mend")
				      	                {
			      	                    	editor.putBoolean("Preso", false);
			      	                    	editor.putBoolean("Non preso", false);
			      	                    	editor.putBoolean("Non Ricordo", true);
			      	                    	editor.commit();
			      	                    }
		      	                    }
		      	                });
		                    builder.setPositiveButton("Ok",new DialogInterface.OnClickListener() 
		      				{
		      					public void onClick(DialogInterface dialog,int id) 
		      					{
	      							statusChecked[0] = pref.getBoolean("Preso", false);
	      			      			statusChecked[1] = pref.getBoolean("Non preso", false);
	      			      			statusChecked[2] = pref.getBoolean("Non Ricordo", false);
      			      					
	      							if(statusChecked[0])
	      							{
	      								timeTherapyOperations.updateTimeStatusById(checkUpId, 1);
	      								thisDiarioHolder.ivListIcon.setImageResource(R.drawable.terapie_completata);
	      							}
	      							if(statusChecked[1])
	      							{
	      								timeTherapyOperations.updateTimeStatusById(checkUpId, 0);
	      								thisDiarioHolder.ivListIcon.setImageResource(R.drawable.interrota);
	      							}
	      							if(statusChecked[2])
      								{
	      								timeTherapyOperations.updateTimeStatusById(checkUpId, 2);
	      								thisDiarioHolder.ivListIcon.setImageResource(R.drawable.terapia_anomalia);
      								}
	      							Diario.updateListView(date);
		      					}});
		                    	builder.setNegativeButton("Anullo", new DialogInterface.OnClickListener() 
		                    	{
		            			@Override
		            			public void onClick(DialogInterface dialog, int id) 
		            			{
		            			}
		            		});
		                    levelDialog = builder.create();
		                    levelDialog.show();
		      			}
		        	  }});
    	        break;
    	        
    		case 2:
    			final TerapieViewHolder thisTerapieHolder;
            	thisTerapieHolder = new TerapieViewHolder();
                LayoutInflater inflaterTerapie = (LayoutInflater) thisContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflaterTerapie.inflate(R.layout.terapie_listview_single_item, null);
                thisTerapieHolder.ivListIcon = (ImageView) convertView.findViewById(R.id.ivTerapieListIcon);
                thisTerapieHolder.tvListText = (TextView) convertView.findViewById(R.id.tvTerapieListText);
                thisTerapieHolder.tvDateRange = (TextView) convertView.findViewById(R.id.tvTerapieDateRange);
                
                try {
	                if (Integer.parseInt((String) list.get(position).get(2))==0)
	                { 
	                	thisTerapieHolder.ivListIcon.setImageResource(R.drawable.terapia_anomalia);
	                }
	                if (Integer.parseInt((String) list.get(position).get(2))==2)
	                { 
	                	thisTerapieHolder.ivListIcon.setImageResource(R.drawable.terapie_completata);
	                }
	                if (Integer.parseInt((String) list.get(position).get(2))==1)
	                { 
	                	thisTerapieHolder.ivListIcon.setImageResource(R.drawable.terapie_1);
	                }
                }
                catch(Exception e) {
                	e.printStackTrace();
                	thisTerapieHolder.ivListIcon.setImageResource(R.drawable.terapia_anomalia);
                }
                thisTerapieHolder.ref = position;
                thisTerapieHolder.tvListText.setText(list.get(position).get(0).toString());
                thisTerapieHolder.tvDateRange.setText(list.get(position).get(3).toString() + " dite");
                break;
                
    		case 3:
			    final CalendarViewHolder calendarViewHolder;
            	calendarViewHolder = new CalendarViewHolder();
                LayoutInflater inflaterCalendar = (LayoutInflater) thisContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflaterCalendar.inflate(R.layout.calendario_listview_single_item, null);
                calendarViewHolder.ivListIcon = new ArrayList<ImageView>();
                
                for (Integer value : imageMapped) 
                {
                	calendarViewHolder.ivListIcon.add((ImageView) convertView.findViewById(value));
                }
                calendarViewHolder.tvListText = (TextView) convertView.findViewById(R.id.tvCalendarioNumberView);
                calendarViewHolder.tvMonthName = (TextView) convertView.findViewById(R.id.tvMonth);

                final CalendarObject eachDay = (CalendarObject) list.get(position);
                calendarViewHolder.ref = position;
                calendarViewHolder.tvListText.setText(Integer.toString(eachDay.getDate()));
                
                if (eachDay.getFotoList() != null) 
                {
                	int positionOfImage = 0;
                	
                	for (String fotoResource : eachDay.getFotoList())
                	{
                		calendarViewHolder.ivListIcon.get(positionOfImage).setImageResource(Integer.parseInt(fotoResource));
                		positionOfImage+=1;
                	}
                }
                if (eachDay.isLastDayOfMonth() || position == 0) 
                { 
                	calendarViewHolder.tvMonthName.setText(eachDay.getMonthName());
                	calendarViewHolder.tvMonthName.setVisibility(View.VISIBLE);
                }
                else
                {
                	calendarViewHolder.tvMonthName.setVisibility(View.GONE);
                }
                final CalendarObject firstVisibleDay = (CalendarObject) list.get(Calendario.lvMenu.getFirstVisiblePosition());
                if(firstVisibleDay.isLastDayOfMonth())
                {
                	Calendario.tvMonth.setText(eachDay.getMonthName());
                	calendarViewHolder.tvMonthName.setVisibility(View.GONE);
                }
                final View view =convertView;
                convertView.setOnClickListener(new View.OnClickListener() 
                {
					@Override
					public void onClick(final View view) 
					{
						// TODO Auto-generated method stub
						view.setBackgroundColor(Color.parseColor("#e6e6e6"));
						final Handler handler = new Handler();
						handler.postDelayed(new Runnable() 
						{
						  @Override
						  public void run() 
						  {
							  view.setBackgroundColor(Color.parseColor("#ffffff"));
						  }
						}, 200);
						Calendario.goToSelectedDate(eachDay.getFullDate());
					}
				});
                break;
    	}
        return convertView;
    }
    
    public int getCheckUpId(int position)
    { 
    	int id= -1;
        DiarioObject listObject = (DiarioObject) list.get(position);
        id = listObject.getId();
    	return id;
    }
    
    public static int getIdItem(int position) 
    {
    	String idStrig = list.get(position).get(1).toString();
    	int id = Integer.valueOf(idStrig);
        return id;
    }

	@Override
	public void onClick(View v) 
	{
		// TODO Auto-generated method stub
	}
}
