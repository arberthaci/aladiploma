package com.appforgood.effeci;

import java.lang.reflect.Method;
import java.util.ArrayList;

import com.appforgood.effeci.DialogBatterieCheckBox;
import com.appforgood.effeciObjectModels.BacteriumObject;
import com.appforgood.effeciObjectModels.BacteriumOperations;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.util.Base64;
import android.media.ThumbnailUtils;

public class Profilio extends Fragment implements OnClickListener
{
    View actualView ;
    TextView name, surname;
    public static ToggleButton attivaServizi;
    ToggleButton datiSalute;
    ToggleButton notifiche;
    ImageButton batterioSelect;
    ImageView profilImage;
    CheckBox checkBoxBatterio;
    boolean attivaServiziShPreferences;
    boolean datiSaluteShPreferences;
    boolean notificheShPreferences;
    SharedPreferences preference;
    Bitmap bitmap;
    Method method;
    RoundImage roundedImage ;
    String userName = "";
    String userSurName = "";
    String imagePath;
    public static boolean isOnResume = false;
    LinearLayout batterioContainer;
    ArrayList<BacteriumObject> bacteriumArrayList = new ArrayList<BacteriumObject>();
    ArrayList<String> bacteriumArrayListID = new ArrayList<String>();
    BacteriumObject bacteriumObject = new BacteriumObject();
    static BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    static Context context;
    public static int[] bacteriumIdList;
    static SharedPreferences prefBatterio = null;
    Editor editorBatterio = null;

    @Override
       public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState)
       {
            prefBatterio = this.getActivity().getSharedPreferences("SharedPreferencesEffeciCheckBox", Context.MODE_PRIVATE);
            editorBatterio = prefBatterio.edit();

            BacteriumOperations bacteriumOperations = new BacteriumOperations(getActivity());
            bacteriumArrayList = bacteriumOperations.getAllBacteriums();

            final CharSequence[] batterioList = new String[bacteriumArrayList.size()];
            final boolean[] batterioChecked =  new boolean[bacteriumArrayList.size()];
            bacteriumIdList = new int[bacteriumArrayList.size()];

            for (int i =0 ; i < bacteriumArrayList.size(); i++)
            {
                batterioList[i] = bacteriumArrayList.get(i).getBacteriumName();
                bacteriumIdList[i] = bacteriumArrayList.get(i).getBacteriumId();
            }
            context = this.getActivity();
            preference = this.getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
            attivaServiziShPreferences = preference.getBoolean("attiva_servizzi", false);
            datiSaluteShPreferences = preference.getBoolean("dati_salute", false);
            notificheShPreferences = preference.getBoolean("notifiche", false);

            ActionBar actionBar = getActivity().getActionBar();
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            LayoutInflater inflaterLayout = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View customView = inflaterLayout.inflate(R.layout.profilio_actionbar, null);
            actionBar.setCustomView(customView);
            actionBar.setDisplayShowCustomEnabled(true);

            actualView = inflaterLayout.inflate(R.layout.profilo, null);
            attivaServizi = (ToggleButton) actualView.findViewById(R.id.toggleButton1);
            datiSalute = (ToggleButton) actualView.findViewById(R.id.toggleButton2);
            notifiche = (ToggleButton) actualView.findViewById(R.id.toggleButton3);
            batterioSelect = (ImageButton) actualView.findViewById(R.id.imageButton1);
            profilImage = (ImageView) actualView.findViewById(R.id.ivProfilioImage);
            name = (TextView) actualView.findViewById(R.id.tvProfilioUsername);
            surname = (TextView) actualView.findViewById(R.id.tvProfilioSurName);

            Editor editor = preference.edit();
            String imagePath = preference.getString("imagePreference", null);
            userName = preference.getString("myName", null);
            userSurName = preference.getString("mySurname", null);

            if(userName !=null)
            {
                name.setText(userName);
            }
            if(userSurName !=null)
            {
                surname.setText(userSurName);
            }
            if(imagePath != null)
            {
                bitmap = BitmapFactory.decodeFile(imagePath);
                bitmap = ThumbnailUtils.extractThumbnail(bitmap, 100, 100);
                //Round the image
                roundedImage = new RoundImage(bitmap);
                //Show the images
                profilImage.setImageDrawable(roundedImage);
            }
            batterioSelect.setOnClickListener(this);
            profilImage.setOnClickListener(this);

            if(attivaServiziShPreferences == true)
            {
                attivaServizi.setChecked(true);
            }
            else
            {
                attivaServizi.setChecked(false);
            }

            if(datiSaluteShPreferences == true)
            {
                datiSalute.setChecked(true);
            }
            else
            {
                datiSalute.setChecked(false);
            }
            if(notificheShPreferences == true)
            {
                notifiche.setChecked(true);
            }
            else
            {
                notifiche.setChecked(false);
            }

            attivaServizi.setOnCheckedChangeListener(new OnCheckedChangeListener()
            {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(buttonView.isChecked() == true)
                 {
                        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                        if (mBluetoothAdapter == null)
                    {
                        Toast.makeText(getActivity(), "Kjo pajisje nuk suporton bluetooth-in", Toast.LENGTH_LONG).show();
                    }
                    else
                    {

                		if (!mBluetoothAdapter.isEnabled())
                		{
            	                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            	                startActivityForResult(enableBtIntent, Home.REQUEST_ENABLE_BT);
                		}
                           // Toast.makeText(context, "Rilevamento batteri e attivata ", Toast.LENGTH_SHORT).show();
                            Editor editor = preference.edit();
                            editor.putBoolean("attiva_servizzi", true);
                            editor.commit();
                      }
                    }
                   else
                   {
                      mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                      if (mBluetoothAdapter == null)
                    {
                         Toast.makeText(getActivity(), "Kjo pajisje nuk suporton bluetooth-in", Toast.LENGTH_LONG).show();
                       }
                       if (mBluetoothAdapter.isEnabled())
                       {
                           Toast.makeText(getActivity(), "Zbulimi i pajisjeve me te njejtin aplikacion dhe te njejten semundje eshte šaktivizuar", Toast.LENGTH_SHORT).show();
                           }
                       Editor editor = preference.edit();
                       editor.putBoolean("attiva_servizzi", false);
                       editor.commit();
                   }
                }
            });

            datiSalute.setOnCheckedChangeListener(new OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                   if(buttonView.isChecked() == true)
                   {
                    Editor editor = preference.edit();
                    editor.putBoolean("dati_salute", true);
                    editor.commit();
                    }
                   else
                   {
                    Editor editor = preference.edit();
                       editor.putBoolean("dati_salute", false);
                       editor.commit();
                   }
                }
                });

            notifiche.setOnCheckedChangeListener(new OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                   if(buttonView.isChecked() == true)
                   {
                    Editor editor = preference.edit();
                    editor.putBoolean("notifiche", true);
                    editor.commit();
                    }
                   else
                   {
                    Editor editor = preference.edit();
                       editor.putBoolean("notifiche", false);
                       editor.commit();
                   }
                }
                });

            for(int i = 0; i< batterioChecked.length; i++)
            {
                batterioChecked[i] = prefBatterio.getBoolean(String.valueOf(bacteriumIdList[i]), false);
                editorBatterio.commit();
            }
            batterioContainer = (LinearLayout) actualView.findViewById(R.id.batterioContainer);
            batterioContainer.removeAllViews();
            int j = 0;
            for (int i = 0; i<batterioChecked.length; i++)
            {
                if(batterioChecked[i]== true )
                {
                    if(j<1)
                    {
                        TextView textView = new TextView(getActivity());
                        LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
                        params.setMargins(0, 0, 10, 0);
                        textView.setLayoutParams(params);
                        textView.setText(batterioList[i]);
                        textView.setTextSize(18);
                        textView.setTextColor(Color.parseColor("#a3a3a3"));
                        textView.setTypeface(null, Typeface.BOLD);
                        batterioContainer.addView(textView);
                    }
                    j++;
                }
            }
            if(j>1)
            {
                TextView textView = new TextView(getActivity());
                LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 10, 0);
                textView.setLayoutParams(params);
                textView.setText("...");
                textView.setTextColor(Color.parseColor("#636363"));
                textView.setTypeface(null, Typeface.BOLD);
                batterioContainer.addView(textView);
                editorBatterio.putBoolean("0", false);
            }
            else if(j==0)
            {
                editorBatterio.putBoolean("0", true);
            }
            editorBatterio.commit();
            
            SharedPreferences pref = getActivity().getSharedPreferences("SharedPreferencesEffeci", Context.MODE_PRIVATE);
			boolean notificheShPreferences = pref.getBoolean("notifiche", false);
			if (notificheShPreferences)
			{
				Home.initializeNotification(notificheShPreferences, getActivity());
			}
			
            return actualView;
       }
    
    @Override
    public void onResume()
    {
        super.onResume();
        if(isOnResume)
        {
            imagePath = preference.getString("imagePreference", "-1");
            if(!imagePath.equalsIgnoreCase("-1"))
            {
                bitmap = BitmapFactory.decodeFile(imagePath);
                bitmap = ThumbnailUtils.extractThumbnail(bitmap, 100, 100);
                //Round the image
                roundedImage = new RoundImage(bitmap);
                //Show the images
                profilImage.setImageDrawable(roundedImage);
            }
                userName = preference.getString("myName", "-1");
                name.setText(userName);
                userSurName = preference.getString("mySurname", "-1");
                surname.setText(userSurName);
            }
        isOnResume = false;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imageButton1:
            {
                FragmentTransaction fragment = getActivity().getFragmentManager().beginTransaction();
                DialogBatterieCheckBox dialog = DialogBatterieCheckBox.newInstance();
                dialog.show(fragment, "Tag");
            }
            break;
            case R.id.ivProfilioImage:
            {
                Intent myIntent = new Intent(getActivity(), UploadImage.class);
                getActivity().startActivity(myIntent);
            }
        }
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int wantedWidth, int wantedHeight)
    {
        Bitmap output = Bitmap.createBitmap(wantedWidth, wantedHeight, Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Matrix m = new Matrix();
        m.setScale((float) wantedWidth / bitmap.getWidth(), (float) wantedHeight / bitmap.getHeight());
        canvas.drawBitmap(bitmap, m, new Paint());

        return output;
    }

     static class changeDeviceName extends AsyncTask<String, Void, Void>
     {
        boolean isReady = false;
        String androidName = "";
        @Override
        protected Void doInBackground(String... params)
        {
            androidName = params[0];
            while(!isReady)
            {
                if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON)
                {
                    isReady = true;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            if (isReady)
            {
                mBluetoothAdapter.setName(androidName);
            }
            else
            {
                ///Toast.makeText(context, "ERROR - Bluetooth nome: " + mBluetoothAdapter.getName(), Toast.LENGTH_SHORT).show();
            }
        }
     }
}
