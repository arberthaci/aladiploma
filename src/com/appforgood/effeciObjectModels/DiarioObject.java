package com.appforgood.effeciObjectModels;

public class DiarioObject
{
	String name;
	String value;
	String foto;
	int id; 
	int dailyelementid;
	
	public DiarioObject()
	{
		this.name = "";
		this.value = "";
		this.foto = "";
		this.id = -1;
		this.dailyelementid = -1;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value) 
	{
		this.value = value;
	}
	
	public String getFoto() 
	{
		return foto;
	}

	public void setFoto(String foto) 
	{
		this.foto = foto;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public int getDailyelementid() 
	{
		return dailyelementid;
	}

	public void setDailyelementid(int dailyelementid) 
	{
		this.dailyelementid = dailyelementid;
	}
}
