package com.appforgood.effeciObjectModels;

import android.provider.BaseColumns;

public class DailyElements 
{
	int dailyElementsId;
	String name;
	String activeFoto;
	String pasiveFoto;
	
	public DailyElements()
	{
		this.dailyElementsId = -1;
		this.name = "";
		this.activeFoto = "";
		this.pasiveFoto = "";
	}
	
	public static abstract class DailyElementsTable implements BaseColumns 
	{
        public static final String TABLE_NAME = "dailyElements";
        
        public static final String DAILY_ELEMENTS_ID= "dailyElementsId";
        public static final String NAME = "name";
        public static final String ACTIVE_FOTO = "activeFoto";
        public static final String PASIVE_FOTO = "pasiveFoto";
	}


	public int getDailyElementsId() 
	{
		return dailyElementsId;
	}

	public void setDailyElementsId(int dailyElementsId) 
	{
		this.dailyElementsId = dailyElementsId;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getActiveFoto() 
	{
		return activeFoto;
	}

	public void setActiveFoto(String activeFoto) 
	{
		this.activeFoto = activeFoto;
	}

	public String getPasiveFoto() 
	{
		return pasiveFoto;
	}

	public void setPasiveFoto(String pasiveFoto) 
	{
		this.pasiveFoto = pasiveFoto;
	}
}
	
	
	
	
	
