package com.appforgood.effeciObjectModels;

import android.provider.BaseColumns;

public class GoogleFitObject {
	int googleFitId;
	String date;
	int steps;
	
	public GoogleFitObject()
	{
		this.googleFitId = -1;
		this.date = "";
		this.steps = 0;
	}
	
	public static abstract class GoogleFitTable implements BaseColumns 
	{
        public static final String TABLE_NAME = "googleFit";
        
        public static final String GOOGLE_FIT_ID= "googleFitId";
        public static final String DATE = "date";
        public static final String STEPS = "steps";
	}

	public int getGoogleFitId() {
		return googleFitId;
	}

	public void setGoogleFitId(int googleFitId) {
		this.googleFitId = googleFitId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getSteps() {
		return steps;
	}

	public void setSteps(int steps) {
		this.steps = steps;
	}
}
