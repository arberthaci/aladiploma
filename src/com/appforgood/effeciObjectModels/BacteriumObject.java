package com.appforgood.effeciObjectModels;

import android.provider.BaseColumns;

public class BacteriumObject 
{
	private int bacteriumId;
	private String bacteriumName;
	private String imagePath;
	private int status;
	
	public BacteriumObject() 
	{
		this.bacteriumId = 2;
		this.bacteriumName = "";
		this.imagePath = "";
		this.status = 1;
	}

	public static abstract class BacteriumTable implements BaseColumns 
	{
        public static final String TABLE_NAME = "bacterium";
        
        public static final String BACTERIUM_ID = "bacteriumId";
        public static final String BACTERIUM_NAME = "bacteriumName";
        public static final String IMAGE_PATH = "imagePath";
        public static final String STATUS = "status";
    }

	public int getBacteriumId() 
	{
		return bacteriumId;
	}

	public void setBacteriumId(int bacteriumId) 
	{
		this.bacteriumId = bacteriumId;
	}

	public String getBacteriumName() 
	{
		return bacteriumName;
	}

	public void setbacteriumName(String bacteriumName) 
	{
		this.bacteriumName = bacteriumName;
	}
    
	public String getImagePath() 
	{
		return imagePath;
	}

	public void setImagePath(String imagePath) 
	{
		this.imagePath = imagePath;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status) 
	{
		this.status = status;
	}
}
