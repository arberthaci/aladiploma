package com.appforgood.effeciObjectModels;
import android.provider.BaseColumns;

public class TimeTherapyObject 
{
	private int timeId;
	private long therapyId;
	private String dateId;
	private String time;
	private int status;
	
	public TimeTherapyObject() 
	{
		this.timeId = -1;
		this.therapyId = -1;
		this.dateId = "";
		this.time = "";
		this.status = 1;
	}
	
	public static abstract class TimeTable implements BaseColumns 
	{
        public static final String TABLE_NAME = "time";
        
        public static final String TIME_ID = "timeId";
        public static final String THERAPY_ID_REF = "therapyId";
        public static final String DATE_ID = "dateId";
        public static final String TIME = "time";
        public static final String STATUS = "status";
    }

	public int getTimeId() 
	{
		return timeId;
	}

	public void setTimeId(int timeId) 
	{
		this.timeId = timeId;
	}

	public long getTherapyId() 
	{
		return therapyId;
	}

	public void setTherapyId(long therapyId) 
	{
		this.therapyId = therapyId;
	}

	public String getDateId() 
	{
		return dateId;
	}

	public void setDateId(String dateId) 
	{
		this.dateId = dateId;
	}

	public String getTime() 
	{
		return time;
	}

	public void setTime(String time) 
	{
		this.time = time;
	}

	public int getStatus() 
	{
		return status;
	}
	
	public void setStatus(int status) 
	{
		this.status = status;
	}
}

