package com.appforgood.effeciObjectModels;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BacteriumOperations extends SQLiteOpenHelper 
{
	private static final int DATABASE_VERSION = DbUtils.DATABASE_VERSION;
    private static final String DATABASE_NAME = DbUtils.DATABASE_NAME;
    private SQLiteDatabase varDatabase;

    public BacteriumOperations(Context context) 
    {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    	this.varDatabase = this.getWritableDatabase();
    }
               
    @Override 
    public void onCreate(SQLiteDatabase db) 
    {
        //db.execSQL(SQL_CREATE_TABLE);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        //db.execSQL(SQL_DELETE_TABLE);
        //onCreate(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    public long insertBacterium(BacteriumObject bacteriumObject) 
    {
    	long rowId = 1;
    	try 
    	{
	        ContentValues contentValues = new ContentValues();

	        contentValues.put(BacteriumObject.BacteriumTable.BACTERIUM_NAME, bacteriumObject.getBacteriumName());
	        contentValues.put(BacteriumObject.BacteriumTable.IMAGE_PATH, bacteriumObject.getImagePath());
	        contentValues.put(BacteriumObject.BacteriumTable.STATUS, bacteriumObject.getStatus());
	        rowId = varDatabase.insert(BacteriumObject.BacteriumTable.TABLE_NAME, null, contentValues);
	       
    	} 
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
    
    public ArrayList<BacteriumObject> getAllBacteriums() 
    {
    	ArrayList<BacteriumObject> bacteriumList = new ArrayList<BacteriumObject>();
        Cursor cursor = varDatabase.query(BacteriumObject.BacteriumTable.TABLE_NAME, null, null, null, null, null, null);
        
        while(cursor.moveToNext())
        {
        	BacteriumObject bacterium = new BacteriumObject();
        	bacterium.setBacteriumId(cursor.getInt(0));
        	bacterium.setbacteriumName(cursor.getString(1));
        	bacterium.setImagePath(cursor.getString(2));
        	bacterium.setStatus(cursor.getInt(3));
        	bacteriumList.add(bacterium);
        }
        cursor.close();
        
        return bacteriumList;
    }
    
    public BacteriumObject getBacteriumByID(int bacteriumId) 
    {
    	BacteriumObject bacterium = new BacteriumObject();
        Cursor varCursor = varDatabase.query(BacteriumObject.BacteriumTable.TABLE_NAME, null,
        		BacteriumObject.BacteriumTable.BACTERIUM_ID + "= ?" ,
                new String[]{Integer.toString(bacteriumId)}, null, null, null);
        while(varCursor.moveToNext())
        {
        	bacterium.setBacteriumId(varCursor.getInt(0));
        	bacterium.setbacteriumName(varCursor.getString(1));
        	bacterium.setImagePath(varCursor.getString(2));
        	bacterium.setStatus(varCursor.getInt(3));
        }
        varCursor.close();
        
        return bacterium;
    }
    
    public long updateBacterium(BacteriumObject bacterium) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(BacteriumObject.BacteriumTable.BACTERIUM_ID, bacterium.getBacteriumId());
	        contentValues.put(BacteriumObject.BacteriumTable.BACTERIUM_NAME, bacterium.getBacteriumName());
	        contentValues.put(BacteriumObject.BacteriumTable.IMAGE_PATH, bacterium.getImagePath());
	        contentValues.put(BacteriumObject.BacteriumTable.STATUS, bacterium.getStatus());
	        rowId = varDatabase.update(BacteriumObject.BacteriumTable.TABLE_NAME, contentValues, BacteriumObject.BacteriumTable.BACTERIUM_ID + "=" + bacterium.getBacteriumId(), null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
    
    public long updateBacteriumStatus(String bacterium, boolean statusBoolean) 
    {
    	long rowId = -1;
    	int status;
    	if(statusBoolean == true)
    	{
    		status = 1;
    	}
    	else
    	{
    		status = 0;
    	}
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	
	        contentValues.put(BacteriumObject.BacteriumTable.STATUS, status);
	        rowId = varDatabase.update(BacteriumObject.BacteriumTable.TABLE_NAME, contentValues, BacteriumObject.BacteriumTable.BACTERIUM_NAME + " = '" + bacterium + "'", null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace(); 
    	}
    	
        return rowId;  
    }
    
    public long deleteBacterium(int bacteriumID) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(BacteriumObject.BacteriumTable.STATUS, 0);
	        varDatabase.update(BacteriumObject.BacteriumTable.TABLE_NAME, contentValues, BacteriumObject.BacteriumTable.BACTERIUM_ID + "=" + bacteriumID, null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
}