package com.appforgood.effeciObjectModels;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbUtils extends SQLiteOpenHelper 
{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "effeci";
    private static final String SQL_CREATE_TABLE_BACTERIUM = "CREATE TABLE " + BacteriumObject.BacteriumTable.TABLE_NAME + " ("
    		+ BacteriumObject.BacteriumTable.BACTERIUM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
    		+ BacteriumObject.BacteriumTable.BACTERIUM_NAME + " TEXT, "
    		+ BacteriumObject.BacteriumTable.IMAGE_PATH + " TEXT, "
    		+ BacteriumObject.BacteriumTable.STATUS + " INTEGER"
    		+ ")";
    private static final String SQL_CREATE_TABLE_THERAPY = "CREATE TABLE " + TherapyObject.TherapyTable.TABLE_NAME + " ("
    		+ TherapyObject.TherapyTable.THERAPY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
    		+ TherapyObject.TherapyTable.THERAPY_NAME + " TEXT, "
    		+ TherapyObject.TherapyTable.START_DATE + " TEXT, "
    		+ TherapyObject.TherapyTable.DURATION + " INTEGER, "
    		+ TherapyObject.TherapyTable.FREQUENCY + " INTEGER, "
    		+ TherapyObject.TherapyTable.NOTES + " TEXT, "
    		+ TherapyObject.TherapyTable.STATUS + " INTEGER"
    		+ ")";
    private static final String SQL_CREATE_TABLE_DAILY_ELEMENTS = "CREATE TABLE " + DailyElements.DailyElementsTable.TABLE_NAME + " ("
    		+ DailyElements.DailyElementsTable.DAILY_ELEMENTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
    		+ DailyElements.DailyElementsTable.NAME + " TEXT, "
    		+ DailyElements.DailyElementsTable.ACTIVE_FOTO + " TEXT, "
    		+ DailyElements.DailyElementsTable.PASIVE_FOTO + " TEXT "
    		+ ")";
    private static final String SQL_CREATE_TABLE_DAILY_CHECKUP = "CREATE TABLE " + DailyCheckUp.DailyCheckUpTable.TABLE_NAME + " ("
    		+ DailyCheckUp.DailyCheckUpTable.DAILY_CHECKUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
    		+ DailyCheckUp.DailyCheckUpTable.ID_DAILY_ELEMENTS + " INTEGER, "
    		+ DailyCheckUp.DailyCheckUpTable.VALUE + " TEXT, "
    		+ DailyCheckUp.DailyCheckUpTable.DATE + " TEXT "
    		+ ")";
    private static final String SQL_CREATE_TABLE_TIME = "CREATE TABLE " + TimeTherapyObject.TimeTable.TABLE_NAME + " ("
    		+ TimeTherapyObject.TimeTable.TIME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
    		+ TimeTherapyObject.TimeTable.THERAPY_ID_REF + " INTEGER, "
    		+ TimeTherapyObject.TimeTable.DATE_ID + " TEXT, "
    		+ TimeTherapyObject.TimeTable.TIME + " TEXT, "
    		+ TimeTherapyObject.TimeTable.STATUS + " INTEGER "
    		+ ")";
    private static final String SQL_CREATE_TABLE_GOOGLE_FIT = "CREATE TABLE " + GoogleFitObject.GoogleFitTable.TABLE_NAME + " ("
    		+ GoogleFitObject.GoogleFitTable.GOOGLE_FIT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
    		+ GoogleFitObject.GoogleFitTable.DATE + " TEXT, "
    		+ GoogleFitObject.GoogleFitTable.STEPS + " INTEGER"
    		+ ")";
    private static final String SQL_DELETE_TABLE_BACTERIUM = "DROP TABLE IF EXISTS " + BacteriumObject.BacteriumTable.TABLE_NAME;
    private static final String SQL_DELETE_TABLE_THERAPY = "DROP TABLE IF EXISTS " + TherapyObject.TherapyTable.TABLE_NAME;
    private static final String SQL_DELETE_TABLE_DAILY_ELEMENTS = "DROP TABLE IF EXISTS " + DailyElements.DailyElementsTable.TABLE_NAME;
    private static final String SQL_DELETE_TABLE_DAILY_CHECKUP = "DROP TABLE IF EXISTS " + DailyCheckUp.DailyCheckUpTable.TABLE_NAME;
    private static final String SQL_DELETE_TABLE_TIME = "DROP TABLE IF EXISTS " + TimeTherapyObject.TimeTable.TABLE_NAME;
    private static final String SQL_DELETE_TABLE_GOOGLE_FIT = "DROP TABLE IF EXISTS " + GoogleFitObject.GoogleFitTable.TABLE_NAME;
    private SQLiteDatabase varDatabase;

    public DbUtils(Context context) 
    {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    	this.varDatabase = this.getWritableDatabase();
    }
               
    @Override 
    public void onCreate(SQLiteDatabase db) 
    {
        db.execSQL(SQL_CREATE_TABLE_BACTERIUM);
        db.execSQL(SQL_CREATE_TABLE_THERAPY);
        db.execSQL(SQL_CREATE_TABLE_DAILY_ELEMENTS);
        db.execSQL(SQL_CREATE_TABLE_DAILY_CHECKUP);
        db.execSQL(SQL_CREATE_TABLE_TIME);
        db.execSQL(SQL_CREATE_TABLE_GOOGLE_FIT);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        db.execSQL(SQL_DELETE_TABLE_BACTERIUM);
        db.execSQL(SQL_DELETE_TABLE_THERAPY);
        db.execSQL(SQL_DELETE_TABLE_DAILY_ELEMENTS);
        db.execSQL(SQL_DELETE_TABLE_DAILY_CHECKUP);
        db.execSQL(SQL_DELETE_TABLE_TIME);
        db.execSQL(SQL_DELETE_TABLE_GOOGLE_FIT);
        onCreate(db);
    }
}
