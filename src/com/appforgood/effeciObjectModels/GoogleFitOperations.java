package com.appforgood.effeciObjectModels;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class GoogleFitOperations extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = DbUtils.DATABASE_VERSION;
    private static final String DATABASE_NAME = DbUtils.DATABASE_NAME;
    private SQLiteDatabase varDatabase;

    public GoogleFitOperations(Context context) 
    {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    	this.varDatabase = this.getWritableDatabase();
    }
               
    @Override 
    public void onCreate(SQLiteDatabase db) 
    {
        //db.execSQL(SQL_CREATE_TABLE);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        //db.execSQL(SQL_DELETE_TABLE);
        //onCreate(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    public long insertGoogleFit(GoogleFitObject googleFit) 
    {
    	long rowId = 1;
    	try 
    	{
	        ContentValues contentValues = new ContentValues();

	        contentValues.put(GoogleFitObject.GoogleFitTable.DATE, googleFit.getDate());
	        contentValues.put(GoogleFitObject.GoogleFitTable.STEPS, googleFit.getSteps());
	        rowId = varDatabase.insert(GoogleFitObject.GoogleFitTable.TABLE_NAME, null, contentValues);
	       
    	} 
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
        return rowId; 
    }
    
    public ArrayList<GoogleFitObject> getAllGoogleFit() 
    {
    	ArrayList<GoogleFitObject> googleFitList = new ArrayList<GoogleFitObject>();
        Cursor cursor = varDatabase.query(GoogleFitObject.GoogleFitTable.TABLE_NAME, null, null, null, null, null, null);
        while(cursor.moveToNext())
        {
        	GoogleFitObject googleFit = new GoogleFitObject();
        	googleFit.setGoogleFitId((cursor.getInt(0)));
        	googleFit.setDate(cursor.getString(1));
        	googleFit.setSteps(cursor.getInt(2));
        	googleFitList.add(googleFit);
        }
        
        if(cursor != null)
        {
        	cursor.close();
        }
        return googleFitList;
    }
    
    public GoogleFitObject getGoogleFitByDate(String date) 
    {
    	GoogleFitObject googleFit = new GoogleFitObject();
        Cursor cursor = varDatabase.query(GoogleFitObject.GoogleFitTable.TABLE_NAME, null, 
        		GoogleFitObject.GoogleFitTable.DATE + "= ?", new String[]{date}, null, null, null);
        while(cursor.moveToNext())
        {
        	googleFit.setGoogleFitId((cursor.getInt(0)));
        	googleFit.setDate(cursor.getString(1));
        	googleFit.setSteps(cursor.getInt(2));
        }
        
        if(cursor != null)
        {
        	cursor.close();
        }
        return googleFit;
    }
}
