package com.appforgood.effeciObjectModels;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TimeTherapyOperations extends SQLiteOpenHelper 
{
	private static final int DATABASE_VERSION = DbUtils.DATABASE_VERSION;
    private static final String DATABASE_NAME = DbUtils.DATABASE_NAME;
    private SQLiteDatabase database;

    public TimeTherapyOperations(Context context) 
    {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    	this.database = this.getWritableDatabase();
    }
               
    @Override 
    public void onCreate(SQLiteDatabase db) 
    {
        //db.execSQL(SQL_CREATE_TABLE);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        //db.execSQL(SQL_DELETE_TABLE);
        //onCreate(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    public long insertTime(TimeTherapyObject time) 
    {
    	long rowId = -1;
    	try 
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(TimeTherapyObject.TimeTable.THERAPY_ID_REF, time.getTherapyId());
	        contentValues.put(TimeTherapyObject.TimeTable.DATE_ID, time.getDateId());
	        contentValues.put(TimeTherapyObject.TimeTable.TIME, time.getTime());
	        contentValues.put(TimeTherapyObject.TimeTable.STATUS, time.getStatus());
	        rowId = database.insert(TimeTherapyObject.TimeTable.TABLE_NAME, null, contentValues);
    	} 
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
    
    public ArrayList<TimeTherapyObject> getTimeByID(int therapyId)
    {
        ArrayList<TimeTherapyObject> timeList = new ArrayList<TimeTherapyObject>();
        Cursor cursor = database.query(TimeTherapyObject.TimeTable.TABLE_NAME, null,
                TimeTherapyObject.TimeTable.THERAPY_ID_REF + "= ?" ,
                new String[]{Integer.toString(therapyId)}, null, null, null);
        while(cursor.moveToNext())
        {
            TimeTherapyObject timeObject = new TimeTherapyObject();
            timeObject.setTherapyId(cursor.getInt(1));
            timeObject.setDateId(cursor.getString(2));
            timeObject.setTime(cursor.getString(3));
            timeObject.setStatus(cursor.getInt(4));
            timeList.add(timeObject);
        }
        cursor.close();
        
        return timeList;
    }
    public TimeTherapyObject getTimeElementStatusByID(int timeId)
    {
    	TimeTherapyObject timeObject = new TimeTherapyObject();
        Cursor cursor = database.query(TimeTherapyObject.TimeTable.TABLE_NAME, null,
                TimeTherapyObject.TimeTable.TIME_ID + "= ?" ,
                new String[]{Integer.toString(timeId)}, null, null, null);
        while(cursor.moveToNext())
        {
            timeObject.setTimeId(cursor.getInt(0));
            timeObject.setTherapyId(cursor.getInt(1));
            timeObject.setDateId(cursor.getString(2));
            timeObject.setTime(cursor.getString(3));
            timeObject.setStatus(cursor.getInt(4));
        }
        cursor.close();
        
        return timeObject;
    }

    public ArrayList<TimeTherapyObject> getTimeByDate(String dateTherapy)
    {
        ArrayList<TimeTherapyObject> timeList = new ArrayList<TimeTherapyObject>();
        String therapyQuery = "SELECT t.timeId, t.therapyId , t.dateId, t.time, t.status  "
        		+ "FROM therapy th INNER JOIN time t on th.therapyId = t.therapyId  WHERE  t.dateId = '" + dateTherapy + "' ORDER BY t.time DESC";
    	Cursor cursor = database.rawQuery(therapyQuery, null); 

        while(cursor.moveToNext())
        {
            TimeTherapyObject varTime = new TimeTherapyObject();
            varTime.setTimeId(cursor.getInt(0));
            varTime.setTherapyId(cursor.getInt(1));
            varTime.setDateId(cursor.getString(2));
            varTime.setTime(cursor.getString(3));
            varTime.setStatus(cursor.getInt(4));
            timeList.add(varTime);
        }
        cursor.close();
        return timeList;
    }
    
    public ArrayList<TimeTherapyObject> getAllTime() 
    {
    	ArrayList<TimeTherapyObject> timeList = new ArrayList<TimeTherapyObject>();
        Cursor cursor = database.query(TimeTherapyObject.TimeTable.TABLE_NAME, null,
        		TimeTherapyObject.TimeTable.STATUS + " = 1", null, null, null, null);
        while(cursor.moveToNext())
        {
        	TimeTherapyObject timeObject = new TimeTherapyObject();
        	timeObject.setTimeId(cursor.getInt(0));
        	timeObject.setTherapyId(cursor.getInt(1));
        	timeObject.setDateId(cursor.getString(2));
        	timeObject.setTime(cursor.getString(3));
        	timeObject.setStatus(cursor.getInt(4));
        	timeList.add(timeObject);
        }
        cursor.close();
        
        return timeList;
    }
  
    
    public long updateTimeStatus(String time, int status) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(TimeTherapyObject.TimeTable.STATUS, status);
	        rowId = database.update(TimeTherapyObject.TimeTable.TABLE_NAME, contentValues, TimeTherapyObject.TimeTable.TIME + " = '" + time + "'", null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace(); 
    	}
    	
        return rowId; 
    }
    
    public long updateTimeStatusById(int id, int status) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(TimeTherapyObject.TimeTable.STATUS, status);
	        rowId = database.update(TimeTherapyObject.TimeTable.TABLE_NAME, contentValues, TimeTherapyObject.TimeTable.TIME_ID + " = " + id, null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace(); 
    	}
    	
        return rowId; 
    }
    
    public long deleteTime(int timeID) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(TimeTherapyObject.TimeTable.STATUS, 0);
	        database.update(TimeTherapyObject.TimeTable.TABLE_NAME, contentValues, TimeTherapyObject.TimeTable.TIME_ID + "=" + timeID, null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
}
