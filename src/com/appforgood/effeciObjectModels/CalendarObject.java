package com.appforgood.effeciObjectModels;

import java.util.ArrayList;

public class CalendarObject 
{
	int date;
	boolean isLastDayOfMonth;
	String fullDate;
	String monthName;
	ArrayList<String> fotoList;
	
	public CalendarObject () 
	{
		this.date = -1;
		this.fullDate = "";
		this.fotoList = null;
	}

	public int getDate() 
	{
		return date;
	}

	public void setDate(int date) 
	{
		this.date = date;
	}

	public String getFullDate() 
	{
		return fullDate;
	}

	public void setFullDate(String fullDate) 
	{
		this.fullDate = fullDate;
	}

	public ArrayList<String> getFotoList() 
	{
		return fotoList;
	}

	public void setFotoList(ArrayList<String> fotoList) 
	{
		this.fotoList = fotoList;
	}

	public boolean isLastDayOfMonth() 
	{
		return isLastDayOfMonth;
	}

	public void setLastDayOfMonth(boolean isLastDayOfMonth) 
	{
		this.isLastDayOfMonth = isLastDayOfMonth;
	}

	public String getMonthName() 
	{
		return monthName;
	}

	public void setMonthName(String monthName) 
	{
		this.monthName = monthName;
	}
}
