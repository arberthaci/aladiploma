package com.appforgood.effeciObjectModels;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DailyCheckUpOperations extends SQLiteOpenHelper 
{
	private static final int DATABASE_VERSION = DbUtils.DATABASE_VERSION;
    private static final String DATABASE_NAME = DbUtils.DATABASE_NAME;
    private SQLiteDatabase varDatabase;

    public DailyCheckUpOperations(Context context) 
    {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    	this.varDatabase = this.getWritableDatabase();
    }
               
    @Override 
    public void onCreate(SQLiteDatabase db) 
    {
        //db.execSQL(SQL_CREATE_TABLE);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        //db.execSQL(SQL_DELETE_TABLE);
        //onCreate(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    public long insertDailyCheckUp(DailyCheckUp varElements) 
    {
    	long rowId = 1;
    	try 
    	{
	        ContentValues varContentValues = new ContentValues();
	        
	        varContentValues.put(DailyCheckUp.DailyCheckUpTable.ID_DAILY_ELEMENTS, varElements.getIdDailyElements());
	        varContentValues.put(DailyCheckUp.DailyCheckUpTable.VALUE, varElements.getValue());
	        varContentValues.put(DailyCheckUp.DailyCheckUpTable.DATE, varElements.getDate());
	        rowId = varDatabase.insert(DailyCheckUp.DailyCheckUpTable.TABLE_NAME, null, varContentValues);
    	} 
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
    
    public DailyCheckUp getCheckUp(String date, int varId) 
    {
    	DailyCheckUp checkUp = new DailyCheckUp();
        Cursor varCursor = varDatabase.query(DailyCheckUp.DailyCheckUpTable.TABLE_NAME, null,
        		DailyCheckUp.DailyCheckUpTable.ID_DAILY_ELEMENTS + "= ? AND " + DailyCheckUp.DailyCheckUpTable.DATE + " = ?",
                new String[]{Integer.toString(varId), date}, null, null, null);
        while(varCursor.moveToNext())
        {
        	checkUp.setDailyCheckUpId(varCursor.getInt(0));
        	checkUp.setIdDailyElements(varCursor.getInt(1));
        	checkUp.setValue(varCursor.getString(2));
        }
        if(varCursor != null)
        {
        	varCursor.close();
        }
        
        return checkUp;
    }
    
    public DailyCheckUp getCheckUpById(int id) 
    {
    	DailyCheckUp checkUp = new DailyCheckUp();
        Cursor varCursor = varDatabase.query(DailyCheckUp.DailyCheckUpTable.TABLE_NAME, null,
        		DailyCheckUp.DailyCheckUpTable.DAILY_CHECKUP_ID + "= ?",
                new String[]{Integer.toString(id)}, null, null, null);
        while(varCursor.moveToNext())
        {
        	checkUp.setDailyCheckUpId(varCursor.getInt(0));
        	checkUp.setIdDailyElements(varCursor.getInt(1));
        	checkUp.setValue(varCursor.getString(2));
        }
        
        return checkUp;
    }
   
    public long updateDailyCheckUp(int id, String value) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues varContentValues = new ContentValues();
	        varContentValues.put(DailyCheckUp.DailyCheckUpTable.VALUE, value);
	        rowId = varDatabase.update(DailyCheckUp.DailyCheckUpTable.TABLE_NAME, varContentValues, DailyCheckUp.DailyCheckUpTable.DAILY_CHECKUP_ID + "=" + id, null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }

    public long deleteCheckUp(int varID) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues varContentValues = new ContentValues();
	        varContentValues.put(DailyCheckUp.DailyCheckUpTable.DATE, 0);
	        varDatabase.update(DailyCheckUp.DailyCheckUpTable.TABLE_NAME, varContentValues, DailyCheckUp.DailyCheckUpTable.DAILY_CHECKUP_ID + "=" + varID, null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
}