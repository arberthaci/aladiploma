package com.appforgood.effeciObjectModels;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DailyElementsOperations extends SQLiteOpenHelper 
{
	private static final int DATABASE_VERSION = DbUtils.DATABASE_VERSION;
    private static final String DATABASE_NAME = DbUtils.DATABASE_NAME;
    private SQLiteDatabase varDatabase;

    public DailyElementsOperations(Context context) 
    {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    	this.varDatabase = this.getWritableDatabase();
    }
               
    @Override 
    public void onCreate(SQLiteDatabase db) 
    {
        //db.execSQL(SQL_CREATE_TABLE);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        //db.execSQL(SQL_DELETE_TABLE);
        //onCreate(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    public long insertDailyElements(DailyElements elements) 
    {
    	long rowId = 1;
    	try 
    	{
	        ContentValues contentValues = new ContentValues();

	        contentValues.put(DailyElements.DailyElementsTable.NAME, elements.getName());
	        contentValues.put(DailyElements.DailyElementsTable.ACTIVE_FOTO, elements.getActiveFoto());
	        contentValues.put(DailyElements.DailyElementsTable.PASIVE_FOTO, elements.getPasiveFoto());
	        rowId = varDatabase.insert(DailyElements.DailyElementsTable.TABLE_NAME, null, contentValues);
	       
    	} 
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
        return rowId; 
    }
    
   public ArrayList<DailyElements> getAllElements() 
    {
    	ArrayList<DailyElements> elementList = new ArrayList<DailyElements>();
        Cursor cursor = varDatabase.query(DailyElements.DailyElementsTable.TABLE_NAME, null, null, null, null, null, null);
        while(cursor.moveToNext())
        {
        	DailyElements element = new DailyElements();
        	element.setDailyElementsId((cursor.getInt(0)));
        	element.setName(cursor.getString(1));
        	element.setActiveFoto(cursor.getString(2));
        	element.setPasiveFoto(cursor.getString(3));
        	elementList.add(element);
        }
        
        if(cursor != null)
        {
        	cursor.close();
        }
        return elementList;
    }
   
   public ArrayList<DiarioObject> getTherapyTimeByDate(String date)
   {
   		ArrayList<DiarioObject> elements = new ArrayList<DiarioObject>();
   		String elementsByDate = 
   			 "SELECT therapy.status as status, time.time as time "
   			+ " FROM time  INNER JOIN therapy  ON time.therapyId = therapy.therapyId "
   			+ " WHERE time.dateId = '" + date + "'";
   		Cursor cursor = varDatabase.rawQuery(elementsByDate, null); 
   		while(cursor.moveToNext())
   		{
	  		DiarioObject tempDiarioObject = new DiarioObject();
	  		tempDiarioObject.setValue(cursor.getString(cursor.getColumnIndex("time")));
	  		tempDiarioObject.setFoto(cursor.getString(cursor.getColumnIndex("status")));
	        elements.add(tempDiarioObject);
   		} 
   		cursor.close();
  	
   		return elements;
   	}
   
    public ArrayList<String> getElementsByDate(String date)
    {
    	ArrayList<String> elements = new ArrayList<String>();
    	String elementsByDate = "SELECT activeFoto FROM dailyElements JOIN dailyCheckUp ON dailyElementsId = idDailyElements"
    			+ " WHERE dailyCheckUp.date = '" + date + "' AND dailyCheckup.value <> '' ";
    	Cursor cursor = varDatabase.rawQuery(elementsByDate, null); 
    	
    	while(cursor.moveToNext())
    	{
	   		String foto = cursor.getString(cursor.getColumnIndex("activeFoto"));
	   		elements.add(foto);
        } 
   	 	cursor.close();
   	 	
   	 	return elements;
    }
   
    public ArrayList<DiarioObject> getTherapyTimesByDate(String date)
    {
    	ArrayList<DiarioObject> elements = new ArrayList<DiarioObject>();
    	String elementsByDate = "SELECT t.timeId as timeId, th.therapyName as therapyName, t.time as time, t.status as status "
    			+ "FROM time t, therapy th "
    			+ "WHERE t.therapyId = th.therapyId "
    			+ "AND t.dateId = '" + date + "'";
    	Cursor cursor = varDatabase.rawQuery(elementsByDate, null); 
   	 	while(cursor.moveToNext())
        {
	   		DiarioObject tempDiarioObject = new DiarioObject();
	   		tempDiarioObject.setId(cursor.getInt(cursor.getColumnIndex("timeId")));
	   		tempDiarioObject.setName(cursor.getString(cursor.getColumnIndex("therapyName")));
	   		tempDiarioObject.setValue(cursor.getString(cursor.getColumnIndex("time")));
	   		tempDiarioObject.setFoto(cursor.getString(cursor.getColumnIndex("status")));
	        elements.add(tempDiarioObject);
	        	
        } 
	   	cursor.close();
	   	return elements;
    }
    
    public long updateDailyElements(DailyElements bacterium) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(DailyElements.DailyElementsTable.NAME, bacterium.getName());
	        contentValues.put(DailyElements.DailyElementsTable.ACTIVE_FOTO, bacterium.getActiveFoto());
	        contentValues.put(DailyElements.DailyElementsTable.PASIVE_FOTO, bacterium.getPasiveFoto());
	        rowId = varDatabase.update(DailyElements.DailyElementsTable.TABLE_NAME, contentValues, DailyElements.DailyElementsTable.DAILY_ELEMENTS_ID + "=" + bacterium.getDailyElementsId(), null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
    
    public long deleteBacterium(int bacteriumID) 
    {
    	long rowId = -1;
    	try
    	{
        ContentValues contentValues = new ContentValues();
        contentValues.put(DailyElements.DailyElementsTable.PASIVE_FOTO, 0);
        varDatabase.update(DailyElements.DailyElementsTable.TABLE_NAME, contentValues, DailyElements.DailyElementsTable.DAILY_ELEMENTS_ID + "=" + bacteriumID, null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
}