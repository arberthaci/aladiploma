package com.appforgood.effeciObjectModels;

import android.provider.BaseColumns;

public class DailyCheckUp 
{
	int dailyCheckUpId;
	int IdDailyElements;
	String value;
	String date;
	
	public DailyCheckUp()
	{
		this.dailyCheckUpId = -1;
		this.IdDailyElements = -1;
		this.value = "";
		this.date = "";
	}
	
	public static abstract class DailyCheckUpTable implements BaseColumns 
	{
		public static final String TABLE_NAME = "dailyCheckUp";
		
		public static final String DAILY_CHECKUP_ID = "dailyCheckUpId";
		public static final String ID_DAILY_ELEMENTS= "idDailyElements";
		public static final String VALUE = "value";
		public static final String DATE = "date";
	}
	
	public int getDailyCheckUpId() 
	{
		return dailyCheckUpId;
	}
	
	public void setDailyCheckUpId(int dailyCheckUpId)
	{
		this.dailyCheckUpId = dailyCheckUpId;
	}

	public int getIdDailyElements() 
	{
		return IdDailyElements;
	}
	
	public void setIdDailyElements(int idDailyElements) 
	{
		IdDailyElements = idDailyElements;
	}
	
	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value) 
	{
		this.value = value;
	}
	
	public String getDate() 
	{
		return date;
	}
	
	public void setDate(String date) 
	{
		this.date = date;
	}
}
