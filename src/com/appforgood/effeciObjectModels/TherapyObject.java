package com.appforgood.effeciObjectModels;

import android.provider.BaseColumns;

public class TherapyObject 
{
	private long therapyId;
	private String therapyName;
	private String startDate;
	private int duration;
	private int frequency;
	private String notes;
	private int status;
	
	public TherapyObject()
	{
		this.therapyId = -1;
		this.therapyName = "";
		this.startDate = "";
		this.duration = -1;
		this.frequency = -1;
		this.notes = "";
		this.status = 1;
	}
	
	public long getTherapyId() 
	{
		return therapyId;
	}

	public void setTherapyId(long therapyId) 
	{
		this.therapyId = therapyId;
	}

	public String getTherapyName() 
	{
		return therapyName;
	}

	public void setTherapyName(String therapyName) 
	{
		this.therapyName = therapyName;
	}

	public String getStartDate() 
	{
		return startDate;
	}

	public void setStartDate(String startDate) 
	{
		this.startDate = startDate;
	}

	public int getDuration() 
	{
		return duration;
	}

	public void setDuration(int duration) 
	{
		this.duration = duration;
	}

	public int getFrequency() 
	{
		return frequency;
	}

	public void setFrequency(int frequency) 
	{
		this.frequency = frequency;
	}

	public String getNotes() 
	{
		return notes;
	}

	public void setNotes(String notes) 
	{
		this.notes = notes;
	}

	public int getStatus() 
	{
		return status;
	}

	public void setStatus(int status) 
	{
		this.status = status;
	}
	
	public static abstract class TherapyTable implements BaseColumns 
	{
        public static final String TABLE_NAME = "therapy";
        
        public static final String THERAPY_ID = "therapyId";
        public static final String THERAPY_NAME = "therapyName";
        public static final String START_DATE = "startDate";
        public static final String DURATION = "duration";
        public static final String FREQUENCY = "frequency";
        public static final String NOTES = "notes";
        public static final String STATUS = "status";
    }
}
