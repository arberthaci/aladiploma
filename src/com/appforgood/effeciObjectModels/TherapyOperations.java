package com.appforgood.effeciObjectModels;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TherapyOperations extends SQLiteOpenHelper 
{
    private static final int DATABASE_VERSION = DbUtils.DATABASE_VERSION;
    private static final String DATABASE_NAME = DbUtils.DATABASE_NAME;
    private SQLiteDatabase varDatabase;

    public TherapyOperations(Context context) 
    {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    	this.varDatabase = this.getWritableDatabase();
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) 
    {
        //db.execSQL(SQL_CREATE_TABLE);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        //db.execSQL(SQL_DELETE_TABLE);
        //onCreate(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    public long insertTherapy(TherapyObject therapyObject) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(TherapyObject.TherapyTable.THERAPY_NAME, therapyObject.getTherapyName());
	        contentValues.put(TherapyObject.TherapyTable.START_DATE, therapyObject.getStartDate());
	        contentValues.put(TherapyObject.TherapyTable.DURATION, therapyObject.getDuration());
	        contentValues.put(TherapyObject.TherapyTable.FREQUENCY, therapyObject.getFrequency());
	        contentValues.put(TherapyObject.TherapyTable.NOTES, therapyObject.getNotes());
	        contentValues.put(TherapyObject.TherapyTable.STATUS, therapyObject.getStatus());
	        rowId = varDatabase.insert(TherapyObject.TherapyTable.TABLE_NAME, null, contentValues);
    	} 
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
    
    public ArrayList<List> getTherapies() 
    {
    	ArrayList<List> therapiesList = new ArrayList<List>();
    	String therapyQuery = "SELECT  therapyName, therapyId , status, frequency FROM therapy ORDER BY startDate DESC";
    	Cursor cursor = varDatabase.rawQuery(therapyQuery, null); 
    	
    	while(cursor.moveToNext())
        {
    		List<String> therapyList = new ArrayList<String>();
    		String therapyName=cursor.getString(cursor.getColumnIndex("therapyName"));
    		String therapyId=cursor.getString(cursor.getColumnIndex("therapyId"));
    		String status=cursor.getString(cursor.getColumnIndex("status"));
    		String frequency=cursor.getString(cursor.getColumnIndex("frequency"));
    		therapyList.add(therapyName);
    		therapyList.add(therapyId);
    		therapyList.add(status);
    		therapyList.add(frequency);
         	therapiesList.add(therapyList);
        } 
    	cursor.close();
    	
    	return therapiesList;
    }
    
    public ArrayList<TherapyObject> getAllTherapies() 
    {
    	ArrayList<TherapyObject> listTherapies = new ArrayList<TherapyObject>();
        Cursor cursor = varDatabase.query(TherapyObject.TherapyTable.TABLE_NAME , null,
        		TherapyObject.TherapyTable.STATUS + " = 1", null, null, null, null);
        while(cursor.moveToNext())
        {
        	TherapyObject therapyObject = new TherapyObject();
        	therapyObject.setTherapyId(cursor.getInt(0));
        	therapyObject.setTherapyName(cursor.getString(1));
        	therapyObject.setStartDate(cursor.getString(2));
        	therapyObject.setDuration(cursor.getInt(3));
        	therapyObject.setFrequency(cursor.getInt(4));
        	therapyObject.setNotes(cursor.getString(5));
        	therapyObject.setStatus(cursor.getInt(6));
        	listTherapies.add(therapyObject);
        }
        cursor.close();
        
        return listTherapies;
    }
    
    public long getLastTherapyId() 
    {
    	long lastId = -1;
    	String queryGetLastId = "SELECT therapyId from therapy order by therapyId DESC limit 1";
    	Cursor cursor = varDatabase.rawQuery(queryGetLastId, null);
    	if (cursor != null && cursor.moveToFirst()) 
    	{
    		lastId = cursor.getLong(0);
    	}
    	return lastId;
	}
    
    public TherapyObject getTherapyByID(int therapyId) 
    {
    	TherapyObject therapyObject = new TherapyObject();
        Cursor cursor = varDatabase.query(TherapyObject.TherapyTable.TABLE_NAME, null,
        		TherapyObject.TherapyTable.THERAPY_ID + "= ?" ,
                new String[]{Integer.toString(therapyId)}, null, null, null);
        while(cursor.moveToNext())
        {
        	therapyObject.setTherapyId(cursor.getInt(0));
        	therapyObject.setTherapyName(cursor.getString(1));
        	therapyObject.setStartDate(cursor.getString(2)); 
        	therapyObject.setDuration(cursor.getInt(3));
        	therapyObject.setFrequency(cursor.getInt(4));
        	therapyObject.setNotes(cursor.getString(5)); 
        	therapyObject.setStatus(cursor.getInt(6));
        }
        cursor.close();
        
        return therapyObject;
    }
    
    public long updateTherapy(TherapyObject therapyObject) 
    {
    	long rowId = -1;
    	try
    	{ 
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(TherapyObject.TherapyTable.THERAPY_NAME, therapyObject.getTherapyName());
	        contentValues.put(TherapyObject.TherapyTable.START_DATE, therapyObject.getStartDate());
	        contentValues.put(TherapyObject.TherapyTable.DURATION, therapyObject.getDuration());
	        contentValues.put(TherapyObject.TherapyTable.FREQUENCY, therapyObject.getFrequency());
	        contentValues.put(TherapyObject.TherapyTable.NOTES, therapyObject.getNotes());
	        contentValues.put(TherapyObject.TherapyTable.STATUS, therapyObject.getStatus());
	        rowId = varDatabase.update(TherapyObject.TherapyTable.TABLE_NAME, contentValues, TherapyObject.TherapyTable.THERAPY_ID + "=" + therapyObject.getTherapyId(), null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
    
    public long deleteTherapy(int therapyID) 
    {
    	long rowId = -1;
    	try
    	{
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(TherapyObject.TherapyTable.STATUS, 0);
	        varDatabase.update(TherapyObject.TherapyTable.TABLE_NAME, contentValues, TherapyObject.TherapyTable.THERAPY_ID + "=" + therapyID, null);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
        return rowId; 
    }
    
}
